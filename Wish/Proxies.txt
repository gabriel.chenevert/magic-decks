### Proxies du mardi

- Convertisseur de devises
- Blood Moon x 2
- Floraison de l'arbre blanc
- Guerre de la dernière alliance
- Sangromancienne
- Sabbat des sangvoûteurs

- Réservoir de flux éthérique
- Archives d'alhammaret
- Souvenir profane
- Fouet d'érébos
- Maître espion gobelin
- Recruiter of the guard

- Grouilletière
- Taquineur effronté
- Tournoyeur de chaînes gobelin
- Brandon fanatique
- Maître-vandale goblin
- Foudre cheatée
- Patron de guerre de la légion

- Esper sentinel
- Goblin Sharpshooter
- Purphoros, dieu des Forges
- Insurrection

- Play with Fire
- Snapcaster Mage
- Aclazotz, Trahison des profondeur
- Domination de congénères
- Bat-

### Magic-ville

- Bordure blanche 3 px
- Largeur 63 mm
- Hauteur 88 mm
