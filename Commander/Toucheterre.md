# Toucheterre

##  Stratégie

- Remplir le cimetière de terrains puis tenter une splendide restauration pour profiter d'une avalanche d'effets toucheterre

## Decklist

### Créatures (25)

- **Thalia et le monstre de Gitrog**
- Ange d'Éméria
- Ange de l'admonition
- Approvisionneuse infatigable
- Baloth écrasebois
- Béhémoth vivace
- Bergère d'Éméria
- Dansebosquet de Skola
- Déferlante de scarabées rhinocéros
- Druide du printemps
- Druidesse du cercle de la terre
- Dryade chercheuse de verdure
- Émissaire viridian
- Excavatrice de Ramunap
- Fracasseliane centaure
- La nécroflore
- Modeleur de monde
- Multani, avatar de la Yavimaya
- Paisseur arboricole
- Shigeki, visionnaire de Jukai
- Témoin intemporel
- Titan solaire
- Traqueuse infatigable
- Uurg, rejeton de Turg
- Vengeur de Zendikar

### Rituels (14)

- Bifurcation de la route
- Croissance luxuriante
- Culture
- Danse des boules d'amarante
- Errances lointaines
- Éveil de l'animiste
- Explorer
- Les rêves les plus fous
- Lien brisé
- Noyade dans la fange
- Piller le marais
- Portée du kodama
- Splendide restauration
- Végétation explosive

### Éphémères (5)

- Assolement
- Hersage
- Horrible récupération
- Repos de Jaheira
- Résurgence du Roulis

### Enchantements (8)

- Boseiju atteint le ciel // Branche de Boseiju
- Dans les terres sauvages
- Don primitif
- Expédition du Cœur de Khalni
- La Restauration de Dominaria
- Reconnaissance dans le sous-bois
- Refuge félidar
- Repli vers Éméria
- Repli vers Hagra

### Batailles (1)

- Invasion de Lorwyn // Forces de vanneurs

### Artefacts (4)

- Cadran solaire du voyant
- Gemme de Khalni
- Lamenoire reforgée
- Orbe zuranienne

### Terrains (42)

- Forêt x 14
- Marais x 4
- Plaine x 5

- Champ de lotus
- Champ de ruine
- Cour des Cabaretti
- Dalles de Trokair
- Devanture des Obscura
- Étendues sauvages en évolution
- Ferme à putréfaction des Golgari
- Immensité terramorphe
- Jungle épaisse
- Passage merveilleux
- Paysage difforme
- Paysage myriadaire
- Paysage trompeur
- Sanctuaire de Selesnya
- Surplomb des Riveteurs
- Temple de la fausse divinité
- Temple du cimetière marin
- Terrains des charognards
- Tour de la Rupture

## Statistiques

### Valeurs de mana

- T : 196
- I : 122 (62 %)
- C : 74 (38 %)

### Dévotions

- G : 52 (70 %) p = 2
- W : 13 (18 %) p = 3
- B : 9 (12 %) p = 2

### Dates

- Joué la dernière fois : 21/11/2024

## Notes

### Todo

- Tester pour vérifier jouabilité
- Plus de terrains utilitaires ?
- Quelques gros sorts ?
- Uniformiser basics

### Want

- Gitrog Monster
- Titania, Voice of Gaea
- Orzhov Basilica / Guildless Commons
- Green Sun's Zenith
