# Historiques

## Decklist

### Créatures (29)

- **Jodah, l'Unificateur**
- Ancêtre du Terrier des Fæs
- Arbaaz Mir
- Arni Frontcassé
- Aya d'Alexandrie
- Bell Borca, sergent spectral
- Djeru et Hazoret
- Drana, libératrice de Malakir
- Factionnaire
- Ghalta, tyrante de la ruée
- Grand-Pas, rôdeur du nord
- Griffeboyaux, terreur de Qal Sisma
- Hajar, garde du corps loyal
- Havi, le Père-de-toute-chose
- Itzquinth, ainé de Gishath
- Kutzil, archétype de Malamet
- Mécanopteryx forgé d'or
- Radha, seigneur de guerre de la coalition
- Raff Capashen, mage de navire
- Ratadrabik d'Urborg
- Rienne, ange de la renaissance
- Sarah Jane Smith
- Selvala, exploratrice sur le retour
- Surrak Griffedragon
- Urabrask, le Secret
- Vorinclex, Voix de la Voracité
- Wilson, grizzly raffiné
- Yargle et Multani
- Rograkh, fils de Rohgahh

### Rituels (7)

- Bannir d'Edoras
- Cadeau d'Arwen
- Colère de Dieu
- Puissance de l'animiste
- Salve désastreuse d’Urza
- Traverser l'éternité
- Vision de l'Anneau

### Éphémères (8)

- Bénédiction de Belzenlok
- Conspiration mortelle
- Déferlement du salut
- Homicide
- Mépris selon Ertaï
- Rivalité amicale
- Sale fin
- Vous ne pouvez passer !

### Enchantements (5)

- Annie rejoint l'équipe
- Kellan rejoint l'équipe
- Rakdos rejoint l'équipe
- Titos rejoint l'équipe
- Un Anneau pour les gouverner tous

### Artefacts (11)

- Anneau solaire
- Cachet d'ésotérisme
- Étendard de Sultaï
- Heaume du champion
- Lamenoire reforgée
- Miroir de Galadriel
- Pierre de Guermont
- Podium des héros
- Sablier pendentif de Gerrard
- Shaku usé par l’honneur
- Sphère du commandant

### Terrains (40)

- Forêt x 5
- Île x 3
- Marais x 5
- Montagne x 4
- Plaine x 5
- Abstergo Entertainment
- Arroyo isolé
- Bosquet de Riss
- Catacombes de Sombreau
- Dépression de jungle
- Étendues sauvages en évolution
- Grande salle de la Citadelle
- Havre inexploré
- Lacis luminombre
- Oasis luxuriante
- Paysage myriadaire
- Point de vue exaltant
- Reliquaire de la jungle
- Sables verdoyants
- Sommet du Crânedragon
- Tour de commandement
- Vallée de Moussefeu
- Verger exotique

## Statistiques

### Valeurs de mana

- T : 200
- I : 107 (54 %)
- C : 93 (46 %)

### Dévotions

- F : 23 (25 %) p = 3
- W : 22 (24 %) p = 2
- R : 19 (20 %) p = 2
- B : 18 (19 %) p = 2
- U : 11 (12 %) p = 2
