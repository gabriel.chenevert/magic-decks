# Crânebruyère, tête de gruyère

## Stratégie

- Deck Voltron marqueurs
- Gagner vite avant de se faire tuer
- On peut choisir de l'envoyer en zone de commandant plutôt qu'en main ou dans la librairie !

## Decklist

### Créatures (16)

- Ancien immémoré

- Bagarreur de récupération
- Boa sinueux
- Bridebogle insaisissable
- Capitaine au frappe-devant du conclave
- **Crânebruyère, la Tombe ambulante**
- Danseur sinistre
- Dormeur évolué
- Géant cristallin
- Kami des espoirs susurrés
- Mandeur du vide
- Mentor crépuscroc
- Mentor tapecorne
- Rampetête
- Rampeur à carapace crépusculaire
- Titanoth rex

### Rituels (8)

- Appel du Thanatophile
- Essence d'écorchement
- Glyphe de combat de Malamet
- Invocation des anciens
- Sauvagerie grandissante
- Synchronisation des points de vue
- Tempête mortelle
- Volée de bois

### Éphémères (17)

- Assolement
- Bénédiction de rage de bataille
- Bienfait de Gaia
- Charme de l'archidruide
- Crocs inattendus
- Déferlement revigorant
- Don de la vipère
- Filières soudaines
- Glacement du sang
- Intrusion désastreuse
- Perspicacité du chasseur
- Putréfier
- Résistance collective
- Saccager la ville
- Trophée de l'assassin
- Vigueur surnaturelle
- Voile en peau de serpent

### Enchantements (10)

- Adaptation forcée
- Croissance de l'hydre
- Écailles renforcées
- Nécrosynthèse
- Ombre de lion
- Rancœur
- Se repaître des mourants
- Sixième sens
- Talent de l'aubergiste
- Vraska rejoint l'équipe

### Artefacts (10)

- Anneau de Kalonie
- Anneau de Xathrid
- Bottes de pionnier
- Cachet d'ésotérisme
- Gants de gredin
- Hachoir d'âmes de Tarrian
- Lanterne à calotte luisante
- Longue-vue de l'explorateur
- Masque de Griselbrand
- Talisman du faucheur

### Terrains (38)

- Forêt x 17
- Marais x 10
- Cimetière étouffé
- Dépression de jungle
- Diamond City
- Llanowar revenue à la vie
- Marécage bourgeonnant
- Palais d'opale
- Passage des malandrins
- Sanctuaire de tyrite
- Temple de la maladie
- Terrains de nidification
- Tour de commandement

## Statistiques

### Coûts de mana

- T : 155
- I : 87 (56 %)
- C : 68 (44 %)

### Dévotions

- G : 44,5 (65 %) p = 4
- B : 23,5 (35 %) p = 2

### Infos

- Dernière màj : 2025-03-05
- Dernière partie : 2025-03-06

## Notes

### Todo

- Plus de déplacements de marqueurs ?
- Mutate + counters
- + Façons de le sacrifier pour éviter marqueurs -1/-1, boue ou emprisonnement
- Effets statiques de boost pour survivre aux -1/-1 en début de partie ?
- Terrains à recycler

### Questions

- Tuteur dans cimetière pour scarabée ?
- Ramper ?

### Want

- Undying Evil
- Minas Morgul, Dark Fortress
- Greater Good
- The Ozolith 20 €
- Crop Rotation pour tutorer Terrains de nidification ?
- Daring Fiendbonder
- Feign Death / Undying Malice
