# White Zombies

## Description

### Stratégie

- Thèmes : aggro zombies + défausse
- Fabriquer une armée de zombies qui a envie d'attaquer
- Défausser des cartes pour filtrer le deck et capitaliser
- Chercher Avare aux ossements
- Garder mana pour lancer des éphémères ou créer des zombies à la fin du tour du joueur précédent
- Mulligan : ça prend des terrains et petits zombies pour profiter de Varina T4

### Dates

- Dernière màj : 2025-01-11
- Dernière partie : 2025-01-16

## Decklist

### Créatures (22) dont 16 zombies

- Archifielleux d'Ifnir
- Augure mort-vivant
- Avare aux ossements
- Brigadier étincelant
- Chancelier de lazotèpe
- Chasseur de gros gibier
- Création oubliée
- Horde maudite
- L'Émerveillement
- La Valeur
- Le Dieu-Scarabée
- Liche calculatrice
- Momie d'emprisonnement
- Momie détissée
- Momie suprême
- Muse née des tombes
- Oketra l'Éternelle-déesse
- Ratadrabik d'Urborg
- Servant rétif
- Titan de Littjara
- Tormod, le profanateur
- **Varina, reine liche**

### Batailles (1)

- Invasion d'Amonkhet // Converti de lazotèpe

### Rituels (6)

- Approche du Second Soleil
- Armée des damnés
- Aubaine
- Mélodie lointaine
- Pensées tenaces
- Sacres de déterrement

### Éphémères (15)

- Au nom d'Oketra
- Contresort
- Emprise infernale
- Fini les mensonges
- Formation inébranlable
- Fouille obsessionnelle
- Même pas morte
- Négation
- Obscur flétrissement
- Offrir l'immortalité
- Ombre de la tombe
- Plaque d'armure en lazotèpe
- Réparation de cadavre
- Retour au pays
- Rien que le vent

### Enchantements (9)

- Atelier du fabriquant de poupées // Galerie de porcelaine
- Interminables colonnes de morts
- Moisson au graf
- Pacte du nécromancien
- Perspicacité sans âge de Téfeiri
- Procession consécratice
- Reconnaissance
- Terrains d'entraînement
- Vertu intangible

### Artefacts (9)

- Anneau solaire
- Baguette d'Orcus
- Bottes de piedagile
- Cachet d'ésotérisme
- Jambières d'éclair
- Talisman de dominance
- Talisman de hiérarchie
- Talisman de progrès
- Trône du Dieu-Pharaon

### Terrains (38)

- Île x 9
- Marais x 10
- Plaine x 6

- Champ des morts

- Champ souillé
- Cour retirée
- Dépression engloutie
- Labyrinthe du halo
- Paysage contaminé
- Prairie ruisselante
- Sanctuaire ésotérique
- Temple de l'illumination
- Temple de la tromperie
- Temple du silence
- Tour de commandement
- Voie de l'Ascendance

## Stats

### Valeurs de mana

- T : 196
- I : 114 (58 %)
- C : 82 (42 %)

### Dévotions

- B : 38 (46 %) p = 3
- U : 23 (28 %) p = 2
- W : 21 (26 %) p = 3

## Notes

### Questions

- Portail vers l'au-delà / Don du Dieu-Pharaon ?
- Dark Prophecy ?

### Todo

- Tester
- Clarifier dilemmes défausser a priori
- Pousser aspect tribal / blanc ?
- Marquer différences w/ Re-Animator
- Ré-équilibrer terrains (+1 ou 2 marais / -1 ou 2 île / +1 plaine)
- Ajouter interaction / removal (pas que protection)
- Un peu de réanimation quand même (Reanimate / Animate Dead)

### Want

- Dolmen Gate
- Kindred Discovery
- Radiant Destiny
- Dawn's Truce
- Compléter terrains full art AKH
- Alammarret's Archive
- Ancient Excavation
- Dovin's Veto
- Esper Charm
- Etchings of the Chosen
- Grave Scrabbler
- Haunted One
- Tomebound Lich

### Mouais

- Curator of Mysteries
- Geier Reach Sanitarium
- Perplex
- Raffine's Tower
- Stir the Sands

### À rechecker

- Murder of Crows
- Collector's Vault
- Diregraf Captain
- Diregraf Colossus
- Rooftop Storm
- Champion of the Perished
- Shepherd Of Rot (moderne)
- Tainted Adversary
- Acererak the Archlich / Tomb of Annihilation
- Ebondeath, Dracolich
- Phylactery Lich
- Skirk Ridge exhumer
- Forsake the worldly
- Scarab Feast
- Foil
- Damn
- Ominous Seas

### Chase cards

- Crucible of Worlds
- Zombie Master
