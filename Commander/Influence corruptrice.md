# Influence corruptrice

## Résumé

- deck préconstruit légèrement amélioré

## Decklist

### Créatures (24) dont 23 phyrexians

- **Ixhel, scion d'Atraxa**

- Brutaliseur venimeux
- Censeur phyrexian
- Contaminateur de la vigueur
- Corrupteur viridian
- Crânoglodyte biliaire
- Duelliste à mâchoires
- Glissa, héraut du cycle alimentaire
- Greffeur de polluants
- Maître de chapelle de Norn
- Mamba du fléau
- Myr de peste
- Myr sanigriffe
- Oblitérateur phyrexian
- Piqueur de peste
- Rat au ventre de peste
- Rats de sanie
- Récupérateur de Glissa
- Sage de l'évolution
- Seigneur d'essaim phyrexian
- Sheoldred
- Siphonneur pestiféré
- Vecteur de filigrane
- Vishgraz, l'essaim de la mort

### Planeswalkers (1)

- Vraska, la piqûre de trahison

### Rituels (16)

- Assaut délétère
- Caresse de Phyrexia
- Chuchotements nocturnes
- Culture
- Émergence de la cuve
- Enquête infectieuse
- Expulsion impitoyable
- Extension de la sphère
- Flambée de phyrésie
- Fumigation
- Nourrir l'infection
- Recrutement de Geth
- Renaissance phyrexiane
- Restauration surnaturelle
- Rituel sacrificiel
- Tremblement de guivre

### Éphémères (7)

- Appel aux charognards
- Chute de Vraska
- Mortification
- Putréfier
- Réanimation délétère
- Retour au pays
- Réveiller la bête

### Enchantements (3)

- Décret de Norn
- Prison fantomale
- Reboisement de moiselierre

### Artefacts (11)

- Anneau solaire
- Annexe de Norn
- Atlas phyrexian
- Bottes de pionnier
- Cachet d'ésotérisme
- Cachet de Golgari
- Exosquelette greffé
- Lanterne chromatique
- Pierre de Guermont
- Sphère du commandant
- Sphère luisante

### Terrains (38)

- Forêt x 8
- Marais x 6
- Plaine x 6

- Bastion de Karn

- Bois souillé
- Champ souillé
- Citadelle de la steppe de sable
- Lacis luminombre
- Lacis nécrofleur
- Marécage de Bojuka
- Orée de la Krosia
- Paysage myriadaire
- Prairie de Solherbe
- Temple de la maladie
- Temple de la profusion
- Temple du silence
- Tour de commandement
- Verger exotique
- Village fortifié
- Voie de l'Ascendance
- Vue de la canopée

### Jetons

- Bête // Marqueur « poison »
- Phyrexian et Horreur (*/*)
- Phyrexian et Guivre // Phyrexian et Horreur
- Phyrexian et Guivre // Phyrexian et Insecte x 2
- Phyrexian et Puce // Marqueur « poison » x 3
- Phyrexian et Puce // Phyrexian et Insecte x 3

### Sideboard

- Chancreflore
- Fielleux de mycosynthèse
- Inquisiteur de Norn
- Les Vraies Saintes Écritures
- Pourvoyeur de cosse


## Statistiques

### Valeurs de mana

- T : 217
- I : 139 (64 %)
- C : 78 (36 %)

### Dévotions

- B : 32 (41 %) p = 4
- G : 28 (36 %) p = 2
- W : 18 (23 %) p = 2

### Préco

- 82 %
