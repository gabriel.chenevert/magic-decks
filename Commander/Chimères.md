# Chimères

## Stratégie

- Poser petites créatures à mots-clés qui peuvent mourir et meuler le plus possible
- Une fois que Grusilda est là, créer des assemblages rigolos

## Decklist

### Créatures (33)

- __Grusilda, Monster Masher__

- Adepte sinistre
- Ange de la souffrance
- Approvisionneuse du raccommodeur
- Automate sanitaire
- Centaure reparu
- Cératops déchaîné
- Chauve-souris funèbre
- Cheval de cauchemar
- Chien d'Argenfin
- Chuchoteur des cavernes
- Crânoglodyte biliaire
- Démon de répugnance
- Enfant de la nuit
- Espion de balustrade
- Essaim de moucharmes
- Éternelle manticore
- Fantôme hurlant
- Fêtard reparu
- Force funeste
- Glyphe errant volatile
- Goule patricienne
- Hémophage insatiable
- Majordome mort-vivant
- Mamba de Zagoth
- Moissonneur bruyant
- Œuf mystérieux
- Perce-nuages
- Phénix aux plumes perpétuelles
- Psittacérisson
- Shamane aux deux parchemins
- Vermine rongeuse
- Yargle, glouton d'Urborg

### Rituels (9)

- Effondrement en fusion
- Enterré vivant
- Expérience atroce
- Mort vivante
- Nourrir l'essaim
- Obscure requête
- Omnipotence étiolée
- Recrutement effroyable
- Valse macabre

### Éphémères (4)

- Droit à la gorge
- Obscur marché
- Remords accablants
- Triomphe amer

### Enchantements (7)

- Analyse des éclaboussures de sang
- Communion nécrogène
- Courage fongique
- Faveur d'Etali
- Main à percussion
- Over My Dead Bodies
- Réserve du nécromancien

### Artefacts (9)

- Anneau solaire
- Autel sanguinolent
- Babiole du voyageur
- Bottes à lavéperons
- Cachet d'ésotérisme
- Cachet de Rakdos
- Cloche de meneur de goule
- Orbe mesmérique
- Sarbacane du chasseur

### Terrains (38)

- Marais x 18
- Montagnes x 10
- Cavernes du Sacrifice
- Cheminées tourmentées
- Étendues sauvages en évolution
- Forteresse nécropole
- Lande prospère
- Landes de cendres
- Marécage de Bojuka
- Porte de la Falaise
- Porte de la guilde de Rakdos
- Promontoire prospère

## Statistiques

### Valeurs de mana

- T : 189
- I : 122 (65 %)
- C : 67 (35 %)

### Dévotions

- B : 52 (78 %)
- R : 15 (22 %)

## Commentaires

### Todo

- Créatures avec célérité

### Want

- Raucous Theater (surveil land)
