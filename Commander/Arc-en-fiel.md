# Arc-en-fiel

## Decklist

### Créatures (26)

- Ancêtre du Terrier des Fæs
- Archange du Maelstrom
- Archelos, mystique du lagon
- Atla Palani, marraine des nids
- Chevalier de la Nouvelle Alara
- Chromanticore
- Élémental de fusion
- Engeance primitive
- Escouflenfer bicéphale
- Héroïne de la Première circonscription
- Illuna, Zénith des souhaits
- **Jenson Carthalion, exilé druide**
- Messager transguilde
- Moteur laboureur
- Najal, le coureur de l'orage
- Nephilim brillœil
- Néthroï, Zénith de la mort
- O-Kagachi, kami vengeur
- Oracle annelé
- Rienne, ange de la renaissance
- Simulacre solennel
- Sinistre strix
- Sphinx du Pacte des Guildes
- Surrak Griffedragon
- Voyageuse fallaji
- Xyris, la tempête sinuante

### Planeswalker (1)

- Jared Carthalion

### Rituels (13)

- À la recherche de demain
- Cherchauloin
- Désinfection temporelle
- Explorer
- Expulsion impitoyable
- Flammes radieuses
- Lavalanche
- Maelstrom iridien
- Portée du kodama
- Salve des dunes
- Végétation explosive
- Vérités douloureuses
- Voie migratoire

### Éphémères (9)

- Charme d'Abzan
- Charme de Naya
- Charme de Sultaï
- Chemin vers l’exil
- Défrichement sylvestre
- Spirale de croissance
- Terminaison
- Unir la coalition
- Vérité résonnante

### Artefacts (6)

- Cachet d'ésotérisme
- Cornucopée ancienne
- Lanterne chromatique
- Obélisque d’obsidienne
- Pierre de Guermont
- Sphère du commandant

### Enchantements (6)

- Augure prismatique
- Canons à mana
- Emprisonnement par les lignes ley
- Ligne ley du Pacte des Guildes
- Nexus du Maelstrom
- Passage vers l’Arbre-monde

### Terrains (39)

- Forêt x 3
- Île x 2
- Marais x 2
- Montagne x 2
- Plaine x 2
- Avant-poste nomade
- Bivouac de frontière
- Bosquet bruissant
- Carrière de cristal
- Cataractes iridescentes
- Citadelle de la steppe de sable
- Citadelle maritime
- Clairière de scories
- Dépression engloutie
- Étendues sauvages en évolution
- Fosse d'huile de roche
- Herbages
- Immensité terramorphe
- Marécage fumant
- Monastère mystique
- Nécropole croulante
- Orée de la Krosia
- Palais opulent
- Plaine inondée
- Prairie ruisselante
- Reliquaire de la jungle
- Rivière souillée
- Sanctuaire ésotérique
- Terres sauvages
- Tour de commandement
- Vallée forestière
- Verger exotique
- Vue de la canopée

## Statistiques

### Valeurs de mana

- T : 234
- I : 99 (42 %)
- C : 135 (58 %)

### Dévotions

- G : 41 (30 %)
- W : 26,5 (20 %)
- U : 23,5 (17 %)
- R : 22,5 (17 %)
- B : 21,5 (16 %)

### Taux de préco

- 93 %
