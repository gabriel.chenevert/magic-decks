# Loutres

## Decklist

### Créatures (26)

- Alania, tempête divergente
- Archimage émérite
- Baral, directeur de conformité
- Capitaine Tempeste, pillarde de cosmium
- Chanteuse de mistral
- Défense braisecœur
- Djinn de la fontaine
- Duo brûlétincelle
- Écumèbe tyran
- Instructeur piègefoudre
- Kefnet l'Éternel-dieu
- Kitsa, élite de loutreballe
- Kraum, cacophonie assourdissante
- Malcolm les bons yeux
- Mentor attrape-orage
- Mur de l’académie
- Najal, le coureur de l'orage
- Notion obsédante
- Pêcheur de tempête
- Prétentieux Tirecharme
- Primordial diluvien
- Pulvérisateur poing-de-fer
- Pyromanciennes de rodéo
- Skreelix de l’orage
- Sourcier des épaves
- Terreur tolariane

### Planeswalkers (3)

- Jace ravivé
- Jaya, négociatrice ardente
- **Ral, l'intellect crépitant**

### Batailles (1)

- Invasion de Vryn // Anneau du mage surchargé

 ### Rituels (11)

- Assaut d'agate
- Blâme fulminant
- Collet de vide
- Émergence des vagues
- Fracture de la Communauté
- Marée calamiteuse
- Passé enflammé
- Perle de sagesse
- Révélation cosmique
- Tape amicale
- Tir ardent

### Éphémères (15)

- Choc
- Conduction d'électricité
- Dégel
- Déni éblouissant
- Déraillement explosif
- Étherisation
- Pris entre deux feux
- Puissance des humbles
- Ralliement de Vallée
- Réciproquer
- Renflouage
- Salve florissante
- Salve tonnante
- Sélection pour l'inspection
- Sortir les ordures

### Enchantement (1)

- Premier duel de Gadwick

### Terrains (42)

- Forêt (!)
- Île x 23
- Lande
- Montagne x 16
- Village de Rocparoi

## Statistiques

### Valeurs de mana

- T : 184
- I : 102 (55 %)
- C : 82 (45 %)

### Dévotions

- U : 47 (57 %)
- R : 35 (43 %)

## Notes

### Todo

- Remplacer forêt (par proxy ?)
- Rajouter carte
- retirer île / ajouter montagne

### Want

- Village bleu
