# Héritage des légendes

## Résumé

Deck préconstruit amélioré

## Decklist

### Planeswalkers (1)

- **Dihada, plieuse de volontés**

### Créatures (27)

- Adriana, capitaine de la garde
- Ailelame, tyran impérissable
- Alesha, Celle-qui-sourit-devant-la-mort
- Anafenza, esprit de l’arbre-parent
- Arvad le maudit
- Cadric, embraseur d’âme
- Cendreline la pèlerine
- Drana, libératrice de Malakir
- Éomer, maréchal du Rohan
- Etali, Tempête primordiale
- Gonti, seigneur de l'opulence
- Jazal Crinièredor
- Kalitas, traître des Ghet
- Kari Zev, pillarde de vaisseau volant
- Krenko, baron de la rue d’étain
- La Dynamo pèlerine
- Neheb, champion de la Horde de l’effroi
- Odric, maréchal lunarque
- Ratadrabik d'Urborg
- Shanid, fléau des dormeurs
- Sheoldred, l'Apocalypse
- Tajic, lame de la Légion
- Teshar, apôtre de l’Ancêtre
- Traxos, fléau de Kroog
- Verrak, sengien dévoyé
- Zeriam, vent doré
- Zetalpa, Aube primordiale

### Rituels (6)

- Ascension vers la gloire
- Colère de Kaya
- Précepteur diabolique
- Renaissance glorieuse des Archéens
- Salve désastreuse d’Urza
- Sciences environnementales

### Éphémères (7)

- Affres
- Cadeau généreux
- Chute du héros
- Égide des Cieux
- Mortification
- Usure // Déchiqueture
- Vous ne pouvez passer !

### Enchantements (4)

- Jour de la Déstinée
- Le massacre au croc de boucher
- Lever du jour
- Malédiction de conformité

### Artefacts (18)

- Anneau solaire
- Archive hèdron
- Cachet d'ésotérisme
- Épée de l’élu
- Héritage du héros
- Lame du héros
- Lamenoire reforgée
- Le Cercle de Loyauté
- Le hachoir du pillard
- Monument d'Hazoret
- Monument d'Oketra
- Monument de Bontu
- Pierre de Guermont
- Podium des héros
- Pointe de quiétude
- Relique de légendes
- Sphère du commandant
- Tenza, le lacérateur de Godo

### Terrains (37)

- Marais x 5
- Montagne x 5
- Plaine x 6
- Asile de la Chaîne de Geier
- Avant-poste nomade
- Basilique d’Orzhov
- Carnarium de Rakdos
- Falaises de Coupenoire
- Forge de campagne
- Garnison de Boros
- Gorge de Shiv
- Grande salle de la Citadelle
- Immensité terramorphe
- Marécage de Bojuka
- Marécage fumant
- Mikokoro, Centre de la mer
- Ruines angoissantes
- Sanctuaire de tyrite
- Shizo, la réserve de la mort
- Sommet du Crânedragon
- Temple du silence
- Temple du triomphe
- Tour de commandement
- Tour du Reliquaire

## Statistiques

### Valeurs de mana

- T : 221
- I : 144 (65 %)
- C : 77 (35 %)

### Dévotions

- W : 31 (40 %)
- B : 26 (34 %)
- R : 20 (26 %)

### Préco

- 75 %
