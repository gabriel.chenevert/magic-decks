# Tisanes prodigieuses

## Notes

### Stratégie

- Utiliser le pouvoir de Z&D le plus possible en la dégageant
- Profiter des effets toucheterre notamment pour créer des jetons à sacrifier
- Combo : Zimone et Dina + Exécration de sang + Repli vers Casque de corail + Bounceland (ou variations) &ndash; attention pas infini, nombre d'itérations limité par le nombre de cartes restantes dans la bibliothèque
- Gagner en attaquant avec des créatures-terrains indestructibles / armées de jetons (?) ou avec Maze's End ou Laboratory Maniac ou le lézard qui pingne

### Dates

- Dernière partie : 2025-02-08

## Decklist

### Créatures (33)

- **Zimone et Dina**

- Archelos, mystique du lagon
- Colonie de scalaires
- Colosse de la Porte
- Dissident damné 
- Élémental des marées
- Escogriffe niché
- Exécration de sang
- Fidèle de Kiora
- Fouettevigne iridescent
- Guide de circonscription
- Guide kelpie
- Hiérophante germepierre
- Incarnation de la perspicacité
- Indolent des gouffres
- Jolraël, recluse de la Mwônvouli
- Jolraël, voix de Zhalfir
- Lierre grimpe-porte
- Maniaque de laboratoire
- Meloku le Miroir voilé
- Minn, illusionniste rusée
- Némata, gardienne primitive
- Noble féal de noirfiel
- Opportuniste morbide
- Partisan sylvestre
- Ranger de la Scrutaie
- Ranger quirionais
- Tatyova, druidesse benthique
- Tatyova, intendante des marées
- Thallidé tukalanguier
- Vieux sage de la Yavimaya
- Vizir de la chute du sable
- Vrille du Mycotyran
- Zimone et Dina

### Planeswalkers (1)

- Tyvar, bagarreur allègre

### Batailles (1)

- Invasion de Zendikar // Fort céleste éveillé

### Rituels (8)

- Braver les friches
- Éveil sylvestre
- Intervention de Nyléa
- Ouvrez les portes !
- Réapparition
- Regard sylvestre
- Richesse abominable
- Tombe anonyme

### Éphémères (6)

- Charme d'émeraude
- Disperser aux quatre vents
- Éveil de Vitu-Ghazi
- Inversion dramatique
- Lance de mortalité
- Renflouage

### Artefacts (5)

- Bottes de piedagile
- Carafe intarissable
- Dard, la dague étincelante
- Orbe de navigation
- Sceau du patriar

### Enchantements (8)

- Abondance
- Classe : druide
- Reboisement des régions reculées
- Repli vers Casque de corail
- Roulis de Zendikar
- Sommet de la guilde
- Spéléologie
- Terre rugissante

### Terrains (38) dont 8 portes

- Forêt x 7
- Île x 4
- Marais x 2
- Aqueduc de Dimir
- Chambre de croissance des Simic
- Chutes de Boisépine
- Citadelle de sombracier
- Commune des sans guilde
- Dépression de jungle
- Étendues sauvages en évolution
- Ferme à putréfaction des Golgari
- Fin du labyrinthe
- Fondrière mortuaire
- Marigot lugubre
- Palais opulent
- Place des portails
- Pont de Chevêtremare
- Pont de Sombremousse
- Pont de Voûtebrume
- Porte de Gond
- Porte de la guilde de Dimir
- Porte de la guilde de Golgari
- Porte de la guilde de Simic
- Porte de la Mer
- Porte des Manoirs
- Porte du Dragon noir
- Tour de commandement
- Tour du Reliquaire

## Statistiques

### Coûts de mana

- T : 175
- I : 97 (55 %)
- C : 78 (45 %) 

### Dévotions

- G : 44,5 (57 %) p = 2
- U : 21,5 (28 %) p = 2
- B : 12 (15 %) p = 2 

## Notes

### Todo

- Uniformiser portes guildes / bouncelands
- Remplacer cycle de terrains par portes pour WinCo
- out : Meloku ? Nemata ?
- rendre + amusant

### Questions

- Abaisser courbe mana pour démarrage (Elfe Llanovar / oiseau paradis ?)
- Sphère du commandant ?
- Counterspell / Negate
- Plus de création de jetons ?
- Sorts à X ? / gros sorts
- Eloise ?
- Dina ?
- Feeling manque de vert
- Plus de petites créatures avec ETB / effets de mort à sacrifier
- Remettre Champ des morts ?

### Want

- Baldur's Gate
- Everglades
- Jadar, Ghoulcaller of Nephalia
- Jetons calamars
- Jetons tentacule
- Jeton oiseau bleu vert
- Jetons élémentals
- Jetons Parasite
- Rampaging baloths
- Seedborn muse
- Massacre au croc de boucher
- Roil elemental
- Phyrexian arena (good)
- Amulette de vigueur
- Khalni Garden
- Birds of Paradise
- Nine-Fingers Keene

### Try

- Aesi, tyrant du détroit ?
- Awaken keyword
- Archer aux pointes empoisonnées
- Ayara's Oathsworn?
- Blue Sun Zenith ?
- Broken bond
- Exsanguinate
- Freed from the real (untap)
- Grismold, the Dreadsower
- Hédron crab?
- Hydre d'Oran-Rief
- Infernal Tutor ?
- Jungle Wayfinder ?
- Kazandou nectarpot
- Lamenuit de Nadier
- Lay of the land ?
- Machine paradoxale
- Minamo, école de la cascade
- Moteur laboureur ?
- Murkfiend
- Ob Nixilis, the fallen
- Oboro, le palais dans les nuages
- Obscure prophétie
- Prophétesse de Kruphix (bannie)
- Reassembling skeleton
- Rejeton jaddi
- Rites du village (piocher instant)
- Saryth, croc de vipère (untap)
- Sciences environnementales
- Territorial scytecat
- Thornbite staff ?
- Twiddle
- Unbral mantle ? (untap)
- Urne d'éternité ?
- Vertesmanches
- Vraan, Thane exécuteur
