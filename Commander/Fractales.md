# Fractales

## Decklist

### Créatures (22)

- **Kianne, doyenne de la substance // Imbraham, doyen de la théorie**

- Biomathématicien
- Boue mitotique
- Capricope
- Expérience Un
- Ghildmage de Simic
- Grand serpent annoroc
- Imitateur rusé
- Ingénieur de la spirale
- Limon d'Oran-Rief
- Mascotte interdisciplinaire
- Masse inexorable
- Mimique progéniteur
- Observateur souillé
- Pixie de Marafeuille
- Raie bioluminescente
- Sage des manifestations
- Slogurk, suzerain de boue
- Slurrk, l'avale-tout
- Triplés bourrus
- Vorel du Cladus Coque
- Zimone, prodige de Quandrix

### Rituels (12)

- Boue contre l'humanité
- Courbe sinueuse
- Genèse gélatineuse
- Invocation de fractale
- Invocation de ligne ley
- Légion de clones
- Résoudre l'équation
- Révélation shamanique
- Savoir doppelganguer
- Séquence émergente
- Sursimplifier
- Volonté de Sakashima

### Éphémères (15)

- Atterrissage forcé
- Biochangement
- Changer l'équation
- Deuxième récolte
- Éclair de lucidité
- Exploration en commun
- Facétie du polymorphiste
- Impulsion
- Mépris selon Ertaï
- Mutation répulsive
- Option
- Rapetissement
- Revirement
- Spirale de croissance
- Vide gluant

### Enchantement (7)

- Affaire du laboratoire pillé
- Affront miroir
- Don primitif
- Grenouillifier
- Seuil de la providence
- Vision de l'avenir
- Zone paradoxale

### Artefacts (6)

- Cachet de Simic
- Chrysalide du Cartel
- Cryptex
- Matrice de symétrie
- Moteur séquentiel
- Nexus géométrique

### Terrains (38)

- Forêt x 16
- Île x 18
- Campus de Quandrix
- Îlot enchevêtré
- Lacis pâlevigne
- Ruisseau des sylves

## Statistiques

### Valeurs de mana

- T : 199
- I : 109 (55 %)
- C : 90 (45 %)

### Dévotions

- U : 48,5 (54 %)
- G : 41,5 (46 %)

## Notes

### Todo

- Tester
- Remettre 6 cartes
- Fetchs du pauvre pour Sloggurk
- Terrains de nidifcation
- Synergies limon / fractales

### Want

- Fractal Harness
- Rampant Growth
