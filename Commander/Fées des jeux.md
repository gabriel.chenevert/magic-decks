# Fées des jeux

## Stratégie

- Jouer le plus tard possible (éphémère / flash)
- Profiter des synergies fées et taper en vol

### Dates

- Dernière màj : 2025-01-22

## Decklist


### Créatures (29) dont 29 peuples fées volantes

- Alela, conquérante rusée
- Assassin au lamedard
- Butineuse de halo
- Congrégation méprise-sort // Retour à l'envoyeur
- Færie rossignol
- Færie victime de la malédiction de sommeil
- Færies en formation
- Farfadette maîtresse entraveuse
- Farfadette moqueuse
- Garde noire d'Oona
- Imposteur malléable
- Marionnettistes des ombres
- Messager de Tarelion
- Négociateur des Hauts Fæs
- Noble féal du Vallon d'Elendra
- Nuée de færies
- Nymris, escroc d'Oona
- Obyra, duelliste rêvante
- Onirovoleuse færie
- Oona, reine de faeries
- Pillarde de grimoire
- Plaisantin crocheteur de serrure // Libérer la fæ
- Scion d'Oona
- Semeuse de tentation
- Suivantes d'Obyra // Parade désespérée
- **Tegwyll, duc de la splendeur**
- Vilaine des tumulus
- Voyant færie
- Wydwen, la morsure du vent


### Rituels (6)

- Drain d'ego
- Écurage de Tegwyll
- Enlevé par les fæs
- Mélodie lointaine
- Pacte du Grand serpent
- Peur paralysante


### Éphémères (11)

- Chute du héros
- Contresort
- Crachotage de sort
- Dédain de la meute
- Escrime de færies
- Filtration
- Fuite de mana
- Poussière poivrée
- Recherches chahuteuses
- Tromperie selon les færies
- Veilleurs du fortin

### Enchantements (10)

- Âpre fleur
- Curiosité
- Découverte de congénères
- Ligne ley de prescience
- Moqueries des færies
- Protection de témoi
- Recherche de combat
- Reflets de Littjara
- Réverbération de pensée
- Visiteur de la Féerie sauvage

### Artefacts (7)

- Anneau solaire
- Bident de Thassa
- Cachet de Dimir
- Icône de l'Ascendance
- Lanterne guide-âmes
- Pierre d'Erech
- Pierre de l'esprit


### Terrains (37)

- Île x 13
- Marais x 11

- Catacombes de Sombreau

- Conseil des faeries
- Cour retirée
- Dépression engloutie
- Estuaire asphyxié
- Étendues sauvages en évolution
- Île prospère
- Lande prospère
- Marigot lugubre
- Ruisseau éclatant
- Sanctuaire mystique
- Vallon retiré
- Voie de l'Ascendance


## Statistiques


### Valeurs de mana

- T : 192
- I : 110 (57 %)
- C : 82 (43 %)


### Dévotions

- U : 50 (61 %) p = 3
- B : 32 (39 %) p = 2


## Notes


### Questions

- Gain de vie ?


### Want

- Faerie Mastermind ?
- Misleading signpost ? 7,50 €
- Kindred Dominance 5 €
- Faerie Harbinger 4,50 €
- Archmage of echoes 2 €
- Faerie bladecrafter 0,50 / 1,90 €
- Faerie Tauntings 0,30 €
- Tarelion seigneur bienveillant ? 7,50 €
