# Noir c'est noir

aka Proto-Titos

**manque 6 cartes**

## Stratégie

- Faire des affaires noires
- Si possible tutorer Rien n'est jamais perdu et faire défausser
- Sacrifier et réanimer etc.

## Decklist

### Créatures (24)

- Agente sans scrupule

- Annonciateur de la dernière offrande
- Assassin de la Forteresse
- Brise-glaces sibsig
- Chasseuse de titans
- Chien viscéral menaçant
- Détective obstiné
- Duosangsue visqueuse
- Façonneur de peste
- Fielleux caquetant
- Goule sangsucéphale
- Guetteur du Monde souterrain
- Larve charognarde
- Marchand gris d'Asphodèle
- Mur de sang
- Nattes, cauchemar éveillé
- Nécrogoyf
- Parangon des tombeaux béants
- Persécuteur indulgent
- Rat des ruines
- Rats des cryptes
- Revenant
- Squelette interné
- Squelette réassemblable

### Rituels (15)

- Acte crapuleux
- Confiscation cérébrale
- Coût de l'invasion
- Cruauté de Tasigur
- Écheveaux de délire
- Examen attentif
- Fragments d'os
- Obtenir des aveux
- Pas d'échappatoire
- Plaisanterie de Turlupin
- Pourrissement cérébral
- Ratisser l'esprit
- Reconstitution macabre
- Siphonner l'esprit
- Sortir de terre

### Enchantements (8)

- Malédiction de l'intrus
- Malédiction de sangsues // Guetteur sangsue
- Malédiction de sorcellerie
- Oppression
- Rien n'est jamais gâché
- Scion d'Halaster
- Tissage de cadavres
- Torture

### Éphémères (8)

- Blessure nécrotique
- Conviction corrompue
- Homicide
- Ouverture des tombes
- Prix de la célébrité
- Remords accablants
- Secrets du mausolée
- Tourment de venin

### Artefacts (3)

- Capsule de l'exécuteur
- Casse-tête du tourneur sur métal
- Citadelle de Bolas

### Terrains (36)

- Marais x 35
- Champ de ruines

### Sideboard (4)

- Chat noir
- Momie miasmatique
- Sphère du commandant
- Terreur

## Statistiques

### Valeurs de mana

- T : 181
- I : 109 (60 %)
- C : 72 (40 %)

### Dévotions

- B : 72 (100 %) p = 3

## Notes

### Todo

- Transformer en deck Tinybones (défausse / squelettes)
- Clarifier stratégie
- Tester

### Questions

- Plus de squelettes ?
- Terrains utilitaires
- Plus de créatures ?
