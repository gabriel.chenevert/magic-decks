# Gavé comme jamais

## Notes

### Stratégie

- Sacrifices massifs pour punir les adversaires et profiter
- Jouer les capacités de Ghaveh à la dernière minute pour faire des trucs de combat

### Dates

- Dernière partie : 2025-02-08+
- Dernière modification : 2025-02-12

## Decklist

### Créatures (34)

- Ancêtre de la tribu Sakura
- Aron, ruine de Bénalia
- Boa sinueux
- Boucher de Malakir
- Bouffeur de charogne
- Chancreflore
- Chauves-souris de la Forêt Noire
- Elas il-Kor, pèlerine sadique
- Escogriffe putride
- Fertilide
- Foule de scalaires
- **Ghaveh, gourou des spores**
- Goule bouchère
- Grande-Dent, général écureuil
- Kami des espoirs susurrés
- Lamenuit de Nadier
- Limon nécrophage
- Louveteau
- Menace guidecorps
- Mentor du Conclave
- Mikaeus, le lunarque
- Mycoloth
- Mycon d'utopie
- Officiante cruelle
- Opportuniste morbide
- Preneur de chair
- Rishkar, renégat de Peema
- Sage de l'évolution
- Spécialiste en espèces
- Surineur de Zulaport
- Teysa Karlov
- Thrinax hématospore
- Voyant de viscères
- Wombat maudit

### Rituels (10)

- Colère de Dieu
- Colère de Kaya
- Dépopulation
- Exorciser
- Fragments d'os
- Racines assoiffées
- Regard annihilateur
- Restauration surnaturelle
- Rituel sacrificiel
- Symbiose saprobionte

### Éphémères (7)

- Appel inspirateur
- Deuxième récolte
- Fatale glissade
- Mutation d'aura
- Retour au pays
- Rites du village
- Voile en peau de serpent

### Enchantements (5)

- Bastion du souvenir
- Chemin de la découverte
- Écailles renforcées
- Festin des morts victorieux
- Rituel de la moisson de mort

### Artefacts (7)

- Anneau solaire
- Autel d'Ashnod
- Cachet d'ésotérisme
- Dais du sacrifice
- Lanterne à fantôme // Entraver un esprit
- Pincecrâne
- Source de Norn

### Terrains (37)

- Forêt x 8
- Marais x 8
- Plaine x 6

- Bastion de Karn
- Bosquet éclatant
- Bosquet radieux
- Cavernes de Koïlos
- Étendues sauvages en évolution
- Fondrière hantée
- Haut marché
- Landes érodées
- Marécage baigné de soleil
- Ruines de Drannith
- Sables verdoyants
- Temple de la profusion
- Terrains de nidification
- Tour de commandement
- Verger exotique

## Statistiques

### Coûts de mana

- T : 157
- I : 74 (47 %)
- C : 83 (53 %)

### Dévotions

- G : 33 (40 %) p = 2
- B : 30 (36 %) p = 2
- W : 20 (24 %) p = 2

## Notes

### Todo

- Profiter de Undying
- Privilégier éphémères (ex. destroys sacrifice ?)
- Ajuster terrains

### Checker

- Mage du jade ?
- Blood Artist
- Pitiless plunderer ?
- Priest of forgotten gods ?
- Smothering Abomination
- Whisper, blood liturgist ?
- Lire dans les os
- Meathook massacre ?
- Trudge garden ?
- Dryade de jeunes pousses ? 11 €
- Mazirek, nécroprêtre kraul ! 10 €
- Nausée (moderne)
- Brume pestiférée
- Thallidé mortespore
- Pir, imaginative rascal ?
- Ghildmage de Korozda
- Arc de nylea
- Tribute to world tree
- Voltigeuse assoiffée de sang ?
- Black Sun's Zenith
- Dimir's Machination pour transmute
- Young Wolf ?
- Defiler of Vigor ?

### Want

- Cathars' Crusade
- Geralf's Messenger trop pèté
- Grismold
- Rampant Rejuvenator
 
### Hésitations

- Reboisement de moiselierre ?
- Village Rites / Corrupted Conviction ?
