# Stock de provisions

## Decklist

### Créatures (28)

- **Noisette des Floreracines**

- Approvisionneuse infatigable
- Araignée pêche-ciel
- Arasta de la Toile sans fin
- Aubergiste prospère
- Avant-coureurs du Grand Terrassement
- Beledros Flestrefleur
- Chef de bauge honoré
- Colonie de scurrides
- Débandade d'écureuils
- Écureuil affamé
- Ermite du fond des bois
- Escogriffe niché
- Eulogiste de la pierre de lune
- Expert ostéomancien
- Fabricateur de l'académie
- Frugivore insatiable
- Grande-Dent, général écureuil
- Lamenuit de Nadier
- Le Gang du Gland biscornu
- Maître-concocteur de Noisette
- Oie d'or
- Opportuniste morbide
- Puce détraquée
- Sentinelles des cimes
- Souverain écureuil
- Superviseur de la cache d'os
- Toski, porteur de secrets

### Planeswalkers (1)

- Garruk, chasseur maudit

### Rituels (6)

- Apprentissage de mouleracine
- Décret de souffrance
- Massacre de la grouilletière
- Pulsation du Maelstrom
- Révélation shamanique
- Tempête caquetante

### Éphémères (9)

- Déchiqueter
- Deuxième récolte
- Dispute mortelle
- Jugement selon Vent des vertus
- Nocturne de Noisette
- Putréfier
- Saisie de cache
- Scier en deux
- Sonder l'interdit

### Enchantements (9)

- Ascension du maître des bêtes
- Bastion du souvenir
- Emprisonnement des dieux anciens
- Ermite de Bois-manteau
- Havre de Sauleloup
- Nid d'écureuils
- Reboisement de moiselierre
- Sanctuaire d'écureuils
- Talent du gourmand

### Artefacts (9)

- Anneau solaire
- Cachet d'ésotérisme
- Cachet de Golgari
- Crache-glands
- Épée du couinement
- Idole de l'oubli
- Nexus de Masquebois
- Pincecrâne
- Talisman de résistance

### Terrains (38)

- Forêt x 9
- Marais x 8
- Bois souillé
- Cimetière des sylves
- Dépression de jungle
- Étendues sauvages en évolution
- Ferme à putréfaction des Golgari
- Fondrière crépusculaire
- Fondrière hantée
- Grouilletière
- Halliers tranquilles
- Immensité terramorphe
- Lacis nécrofleur
- Lande stérile
- Landes de Llanowar
- Marécage de Bojuka
- Marécage irradié
- Oran-Rief, le Vastebois
- Sinistre forêt sauvage
- Temple de la maladie
- Tour de commandement
- Verger exotique
- Voie de l'Ascendance

## Statistiques

### Valeurs de mana

- T : 196
- I : 124 (63 %)
- C : 72 (37 %)

### Dévotions

- G : 43,5 (60 %)
- B : 28,5 (40 %)

### Préco

- 97 %
