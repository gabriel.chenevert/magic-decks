# Cimetière en effervescence

## Stratégie

- Toucher le plus possible (y compris avec petites créatures) pour créer des Tarmogoyfs
- Les faire grossir en mettant des choses dans les cimetières

## Decklist

### Créatures (28 dont 8 lhurgoyfs)

- Ancêtre de la tribu Sakura

- Approvisionneuse du raccommodeur
- Collectionneur d'yeux
- Coram, le fossoyeur
- Défieur aux Nattes sanguinolentes
- Détritivore
- **Disa la tourmentée**
- Escouflenfer glouton
- Fauxsoyeur
- Ignoble hiérarche
- La Colère
- La Force
- Lhurgoyf
- Magnivore
- Nécrogoyf
- Nécrovore
- Polygoyf
- Prophétesse des crânes
- Proscrit changelin
- Pyrogoyf
- Régisaure pourrissant
- Selvala, coeur des terres sauvages
- Sinistre écorcheur
- Témoin éternel
- Thrinax bourgeonnant
- Tumulugoyf
- Vieux sage de la Yavimaya
- Ziatora, l'incinératrice

### Batailles (1)

- Invasion de Mercadia // Forgeflammes kyren

### Planeswalkers (2)

- Garruk, prédateur du zénith
- Grist, la marée affamée

### Rituels (7)

- Croissance luxuriante
- Dernier acte
- Embrasement de Chandra
- Enterré vivant
- Pillage sans foi
- Pulsation du Maelstrom
- Siphonner l'esprit

### Éphémères (7)

- Charme des Riveteurs
- Commandement de Kolaghan
- Horrible récupération
- Lames de Velis Vel
- Lutter contre le passé
- Marché au clair de lune
- Terminaison

### Artefacts (12 dont 1 lhurgoyf)

- Anneau solaire
- Autel des goyfs
- Babiole du voyageur
- Bottes du zéphyr
- Cachet d'ésotérisme
- Jambière d'éclair
- Le hachoir du pillard
- Nexus de Masquebois
- Récolte prospère
- Talisman d'impulsion
- Talisman d'indulgence
- Talisman de résistance

### Enchantements (4 dont 1 lhurgoyf)

- Couinement de guerre
- Nid du tarmogoyf
- Rituel de la moisson de mort
- Volonté de la nature

### Terrains (39)

- Forêt x 5
- Marais x 4
- Montagne x 3

- Bois souillé

- Canyon croupissant
- Caverne oubliée
- Champ de démolition
- Clairière de scories
- Contreforts d'Ombresang
- Étendues sauvages en évolution
- Garenne au Loup de Kessig
- Halliers protégés
- Halliers tranquilles
- Immensité terramorphe
- Marécage fumant
- Marécage irradié
- Margouillis du Marennois
- Passage merveilleux
- Paysage corrompu
- Paysage myriadaire
- Pic souillé
- Ravin enragé
- Surplomb des Riveteurs
- Temple de l'abandon
- Temple de la maladie
- Temple de la malice
- Terres sauvages
- Tour de commandement
- Vallée de Moussefeu
- Verger exotique

## Statistiques

### Valeurs de mana

- T : 202
- I : 111 (55 %)
- C : 91 (45 %)

### Dévotions

- B : 34 (37 %)
- G : 35 (38 %)
- R : 22 (23 %)

### Préco

- 83 %

### Dates

- Dernière màj : 2024-12-01
- Dernière partie : 2024-12-05 fun !

## Commentaires

### Questions

- Tutorer Nexus ?
- Autres terrains sacrifice 3 couleurs
- Cut Garryk ?
- Cut Altar ?
- Rééquiliber terrains

### Want

- Grim Hireling
- Professional Face-Breaker
- Dragon's Rage Channeler
- Baleful Strix
- Great Furnace
- Seat of the Synod
- Vault of Whispers
- Nethergoyf
- Commercial District
- Champion of Lambholt
