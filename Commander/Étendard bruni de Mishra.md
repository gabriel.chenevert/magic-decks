# Étendard bruni de Mishra

- Dernière partie : ?
- Dernière modification : jamais

## Stratégie

- Préco artefacts ancienne bordure
- Abuser des effets d'entrée et de sortie du champ de bataille ou sacrifice d'artefacts
- Mieux vaut avoir déjà un artefact intéressant à copier quand Mishra arrive
- La règle des légendes ne s'applique pas à la première copie créée par Mishra car nom différent !

## Decklist

### Créatures (22 dont 6 artefacts)

- Anciens de l'atelier
- Ashnod l'Indifférente
- Bagarreur de récupération
- Brudiclad, ingénieur telchor
- Colosse de métallurgie
- Djoïra, capitaine de l'Aquilon
- Embraseur escouflenfer
- Emry, guetteuse du loch
- Escouflenfer du fourneau
- Faïn, le courtier
- Farid, récupérateur entreprenant
- Geth, seigneur du Caveau
- Héraut de l'angoisse
- Maîtresse transmutatrice
- **Mishra, l'éminent**
- Muzzio, architecte visionnaire
- Padeem, consul de l'innovation
- Ratisseur rutilant
- Reforgeuses audacieuses
- Silas Renn, expert chercheur
- Slobad, rétameur gobelin
- Traxos, fléau de Kroog

### Artefacts (30)

- Anneau solaire
- Archive hèdron
- Babiole du voyageur
- Bombe à sortilèges d'annihilation
- Cachet d'ésotérisme
- Cachet de Dimir
- Cachet de Rakdos
- Capsule de l'exécuteur
- Chantier de miroirs
- Comptoir de commerce
- Creuset merveilleux
- Cuve de fonderie
- Dynamo thran
- Échine d'Ish Sah
- Effigie du Dieu-machine
- Enclume du culte des oni
- Hèdron pierre de rêve
- Idole de l'oubli
- Machine lithoforme
- Miroir maudit
- Pierre de Guermont
- Pierre de l'esprit
- Pierre de l'oubli
- Prisme prophétique
- Résonateur strionique
- Schéma de servo
- Source de mycosynthèse
- Source de sanie
- Sphère du commandant
- Sphère mnémonique

### Rituels (6)

- Acte blasphématoire
- Adjuration des pensées
- Destruction de Terisiare
- Itération expressive
- Nourrir l'essaim
- Pillage sans foi

### Éphémères (5)

- Affres
- Distorsion chaotique
- Fait ou fiction
- Fissuration
- Soif de connaissance

### Terrains (37 dont 6 artefacts)

- Île x 5
- Marais x 4
- Montagne x 4
- Aqueduc de Dimir
- Carnarium de Rakdos
- Catacombes de Sombreau
- Caveau des chuchotements
- Chaufferie d'Izzet
- Contreforts d'Ombresang
- Grand Fourneau
- Immensité terramorphe
- Landes cendreuses
- Marécage fumant
- Nécropole croulante
- Paysage myriadaire
- Pont d'Argefalaise
- Pont de Crasseforge
- Pont de Voûtebrume
- Ruine ensevelie
- Siège du Synode
- Temple de la malice
- Temple de la révélation
- Temple de la tromperie
- Tour de commandement
- Tour du Reliquaire
- Verger exotique
- Voie de l'Ascendance

## Jetons (9)

- Construction // Phyrexian et Myr
- Copie // Encrelin
- Copie // Lithoforce x 2
- Eldrazi // Servo
- Encrelin // Formeguerre de Mishra
- Ferraille // Lithoforce
- Ferraille // Phyrexian et Myr
- Lithoforce // Servo

## Statistiques

### Valeurs de mana

- T : 235
- I : 184 (78 %)
- C : 51 (22 %)

### Dévotions

- R : 19 (37 %) p = 2
- U : 17 (33 %) p = 2
- B : 15 (29 %) p = 2

### Taux de préco

- 100 %
