# Gobelins

## Decklist

### Créatures (35 dont 34 gobelins)

- **Krenko le caïd**
- Banneret gobelin
- Chef de guerre gobelin
- Chipeur de trésor
- Commandant des assiégeants
- Coureur de pièges gobelin
- Cratérier gobelin
- Extorqueur des quais
- Finaude de la rue d'étain
- Glaneur gobelin
- Gobelin balafré au combat
- Gobelin des rues
- Gobelin hargneux
- Gobelin hystérique
- Gobeline chef de gang
- Guérillero téméraire
- Guide gobelin
- Incinérateur gemmepaume
- Instigateur gobelin
- Krenko, baron de la rue d'étain
- Lames lézard
- Laquais téméraire
- Maître-vandale gobelin
- Maréchal de guerre mogg
- Matrone gobeline
- Meneur gobelin
- Mentor marquesang
- Patron de guerre de la Légion
- Prospecteur skirkien
- Seigneur bandit hobgobelin
- Skwi, l'immortel
- Spéléologues gobelins
- Taquineur effronté
- Traceur gobelin
- Vétéran de la volée

### Artefacts (4)

- Bannière héraldique
- Cor du héraut
- Icône de l'Ascendance
- Rotefeu gobelin

### Enchantements (6)

- Animosité partagée
- Assaut gobelin
- Bombardement d'assaut
- Bombardement des gobelins
- Oriflamme gobeline
- Secousse d'impact

### Éphémères (12)

- Bûcher funéraire cathartique
- Feu de goudron
- Foudre
- Frappe foudroyante
- Frénésie du pisteur de sorcière
- Héroïsme embrasé
- Incendiez la tour !
- Pile ou face
- Rentrée des classes
- Rituel de Clairepierre
- Ruée dans la pièce
- Vous voyez deux gobelins

### Rituels (5)

- Étreinte de lave
- Rênes du fourneau
- Rôtir
- Surgissement de hordelins

### Terrains (39)

- Montagne x 38
- Cour retirée

### Jetons

- Gobelin x 10

### Sideboard

- Argousin de Krenko
- Gobelin à la peau dure

## Statistiques

### Valeurs de mana

- T : 155
- I : 90 (58 %)
- C : 65 (42 %)

### Dévotions

- R : 65 (100 %)

## Notes

### Want

Proxys :

- Goblin Lackey
- Nykthos, reliquaire de Nyx
- Play with Fire

MagicCorporation :

- Brandon fanatique
- Tournoyeur de chaînes gobelin
- Rotefeu gobelins
