# Soldats

## Decklist

### Créatures (35)

- Armurier vétéran
- Automate évolutif
- Capitaine d'escouade
- Capitaine des veilleurs
- Commandant d'Ornuit
- Commando cathare
- Compagnon bouclier inébranlable
- Contaminateur de la foi
- Delney, vigie débrouillarde
- Écuyer du roi
- Égide des dieux
- Frère d'armes aïnok
- Garde des orties
- Gardienne du ciel intérieur
- Hermine luxlame
- Inspectrice de Thraben
- Mentor des humbles
- Messager du Gondor
- **Myrel, bouclier d'Argive**
- Odric, maître tacticien
- Odric, maréchal lunarque
- Ojer Taq, Fondation des profondeurs
- Parachutiste embusqueur
- Patrouille en deuil // Apparition matinale
- Phalange argiviane
- Renforts résolus
- Serra la rédemptrice
- Survivante de Korlis
- Thalia, gardienne de Thraben
- Tireuse d'élite des Mille lunes
- Trappeur de Gavonie
- Vaillant de Solcastel
- Vétérane de siège
- Vigile de la Nouvelle Bénalia
- Vigile du rituel

### Planeswalker (1)

- Nahiri, la lithomancienne

### Rituels (5)

- Appel du capitaine
- Coup d'état militaire
- Heure du Jugement
- Place à la colère, place à la ruine
- Summum de la gloire

### Éphémères (5)

- Assemblée des fidèles
- Chemin vers l'exil
- Haut les cœurs !
- Ralliement pour le trône
- Résistance divine

### Artefact (4)

- Bâton du magus du soleil
- Hallebarde kor
- Lame ancestrale
- Monument d'Oketra

### Terrains (40)

- Plaine x 40

### Jetons

- Ange
- Humain x 2
- Lame du forgepierre // Zombie
- Les Souterrains
- Soldat x 2

## Statistiques

### Valeurs de mana

- T : 171
- I : 101 (59 %)
- C : 70 (41 %)

### Dévotions

- W : 70 (100 %) p = 3
