# Re-Animator

## Notes

### Stratégie

- Bien mulliganer car courbe de mana bimodale bizarre
- Meuler un max, faire des zombies puis réanimer des grosses créatures pour pas cher
- Secrètement un deck Gisa (plutôt invasion de zombies / grosses créatures que réanimation)
- Winco : sans doute attaquer avec grosses créatures avec piétinement (La Force dans cimetière)

### Dates

- Dernière partie : 2025-02-21
- Dernière màj : 2025-02-01

## Decklist

### Créatures (45)

- **Sidisi, tyran de la Couvée**
- Analyste des répercussions
- Ancêtre de la tribu Sakura
- Araignée vile engeance
- Arbre-goule
- Archigoule de Thraben
- Assistant dément
- Brigadier des damnés
- Carcasse fongiforme
- Chien de l'effroi
- Chien viscéral menaçant
- Craintécharde
- Druide ermite
- Dryade de la belladone
- Elfe des horizons lointains
- Fidèle de Valgavoth
- Fossoyeur abject
- Géant de Lotleth
- Gisa et Geralf
- Gisa, meneuse de goule
- Grenouille au dard empoisonné
- Guidevoie satyre
- Guivre du massacre
- Incarnation du printemps
- Jeune calotte de la mort // Énorme calotte de la mort
- La Force
- La Massacreuse
- Le Vieux Rutstein
- Lhurgoyf d'Urborg
- Libérateur des cryptes
- Liche du Souterrègne
- Murmure, liturgiste de sang
- Nécromasse grouillante
- Némésis des égouts
- Opportuniste morbide
- Prophétesse des crânes
- Sage du reboisement
- Somnophage cruel // Impossible de se réveiller
- Syr Konrad, le Sinistre
- Tisseuse de Nyx
- Titan des tombes
- Troll des tombes golgari
- Valgavoth, dévoreur de terreur
- Vermine vorace
- Vieux brindodactyle

### Rituels (10)

- Brimades
- Conquête des chevaliers-liches
- Émergence frétillante
- Hymne funèbre entraînant
- Paillis
- Persistance
- Réanimation
- Soirée pour les goules
- Tapotements à la fenêtre
- Vie du terreau

### Éphémères (5)

- Plaque d'armure en lazotèpe
- Regard fantasmagorique
- Remords accablants
- Ronger jusqu'à l'os
- Trophée de l'assassin

### Enchantements (3)

- Altération de cimetière
- Animation des morts
- Scion d'Halaster

### Artefacts (2)

- Orbe mesmérique
- Outils de l'embaumeur

### Terrains (34)

- Forêt x 5
- Île x 3
- Marais x 10
- Bosquet prospère
- Cimetière marin de Néphalie
- Côte de la Yavimaya
- Dépression de jungle
- Dépression engloutie
- Étendues sauvages en évolution
- Fondrière mortuaire
- Grotte cachée
- Île prospère
- Lande prospère
- Paysage angoissant
- Pylônes conducteurs
- Site de fouilles de Tocasia
- Temple du cimetière marin
- Tour de commandement
- Verger exotique

## Statistiques

### Valeurs de mana

- T : 222
- I : 133 (60 %)
- C : 89 (40 %)

### Dévotions

- B : 52 (58 %) p = 3

- G : 29 (33 %) p = 1

- U : 8 (9 %) p = 1

## Notes

### Théorie

Si y'a $n$ créatures parmi 100, considérons que la probabilité de piocher une créature est constante $p \approx \frac{n}{100}$ (ce qui est bien sûr faux puisque ça dépend de ce qui reste). Alors la probabilité, en meulant $k$ cartes, de frapper au moins une créature (donc créer un zombie) est $ p_k = 1 - \big( 1 - p \big)^k$ .

- $p_1 = p$ pas assez

- $p_2 = p(2 - p)$ : $51 \, \%$ pour $n = 30$​, $58 \, \%$ pour $n = 35$, $64 \%$ pour $n = 40$, $70 \, \%$ pour $n = 45$

- $p_3 = p(3-3p+p^2)$ : $66 \, \%$ pour $n = 30$, $72 \, \%$ pour $n = 35$, $78 \, \%$ pour $n = 40$, $83 \, \%$ pour $n = 45$

### Ressenti

- Feeling pas assez de sources de mana bleu
- Terrains prospères -> Surveil lands
- Moteur à sacrifice / effets de morts de zombies (ex. Plague Belcher / Wilhelt) ?
- Pas assez de terrains ?
- Un peu plus d'interaction à garder en main
- Manque de mana sinks
- Plus de sources vertes ?
- Attention : dredge n'empêche pas de se decker ! (+ d'effets de récupération de cimetière ?)
- Plus / meilleurs rampeurs ?

### Todo

- Améliorer base mana
- Plus de payoffs zombie
- Protection commandant

### Questions

- Plus d'effets dans cimetière ?
- Moins de cibles de réanimation ? GY tutors ?
- Evoke ?
- Fureteur spireracine ?
- Liche de Havengul ?
- Urrg glouton ?
- Plus de synergies zombies ?
- Tormod pour GY dissuasion ?
- Remettre des mana rocks ?
- Eloise?

### Want

- Fanatic of Rhonas
- Surveil Lands : Hedge Maze / Underground Mortuary
- Lier, Disciple of the Drowned 6 €
- Whip of Erebos 5,50 € ?
- Augur of Autumn 3,50 €
- Apprentice Necromancer 1,00 €
- Reincarnation 0,50 €
- Balustrade Spy 0,15 €
- Life / Death 0,10 €
- Endless obedience \eps
- Doomed Necromancer \eps

### Maybe

- Moldervine reclamation
- Cachet de Golgari
- Blood for Bones ?
- Bramble Familiar ?
- Breach the Multiverse
- Cauldron of Eternity ?
- Crypt of Agadeem
- Deadbridge Chant
- Exhume
- Fiend Artisan
- Greater good
- Grist
- Izoni
- Llanowar Wastes
- Mortivore
- Nighthowler
- Noxious Revival
- Nyléa, déesse de la chasse
- Perpetual Timepiece
- Primal Rage
- Prodigious Growth
- Ramunap Excavator
- Song of Freyalise
- Songs of the damned
- Stinkweed imp
- Sticth Together
- Tainted wood
- Tomb fortress
- Undergrowth stadium
- Varolz
- Vilespawn spider
- Woodland cemetary
- Woodland chasm
- Zopandrel
- Courser of Kruphix

### Chase cards

- Agadeem's Awakening 20 €
