# T'inquiète Ojer

## Stratégie

- booster Ojer Axonil puis pinger les adversaires
- protéger commandant car il sera sans doute ciblé

## Decklist

### Créatures (27)

- **Ojer Axonil, Puissance des profondeurs** // Temple de la force

- Altisaure paniqué

- Canaliseuse de flammes // Incarnation des flammes
- Catapulte indocile
- Contaminateur de l'instinct
- Cracheur de flammes de Kessig
- Duelliste œil-de-lynx
- Épicurien Voldaren
- Équipage de baliste
- Équipage de canon à foudre
- Étincemage rusé
- Forgeflammes d'Erebor
- Frondeur au feu gobelin
- Galopin à lame de scie
- Gobelin des rues
- Gremlin phlyctocracheur
- Grilleur de soirée fanfaron
- Incendiaire gobelin
- Lagac crachefeu
- Livaan, cultiste de Tiamat
- Mentor exigeant
- Molosse de magma de Chandra
- Ojer Axonil, Puissance des profondeurs // Temple de la force
- Piqueur vithien
- Pyromancien scoriacé
- Pyromancien sybarite
- Thermo-alchimiste
- Tournoyeur de chaînes gobelin

### Planeswalker (1)

- Chandra, meneuse de flammes

### Artefacts (15)

- Autel du panthéon
- Bottes de rapidité
- Cachet d'ésotérisme
- Épée sigillée de Valéron
- Fouet sous tension
- Hache de Gimli
- Lame récupérée
- Lamenoire reforgée
- Mine rugissante
- Outil de combustion
- Pioche en diamant
- Rapière de duel
- Rotefeu gobelin
- Sphère du commandant
- Torche solaire

### Enchantements (7)

- Barbelures de mana
- Cité en feu
- Émancipation ardente
- Férocification
- Inscription ardente
- Kumano affronte Kakkazan // Gravure de Kumano
- Puissance tonitruante

### Éphémères (7)

- Activer les flammes
- Choc
- Déchaînement de fureur
- Faire appel à un professionnel
- Fissuration
- Foudre
- Frisson de probabilité

### Rituels (9)

- Confluence ardente
- Coup de flamme
- Danger tectonique
- Détermination de Wrenn
- Éclairer la piste
- Embrocher les critiques
- Fin des festivités
- Mitraille
- Tremblement de terre

### Terrains (34)

- "Montagne" (Île) x 3
- "Montage" (Marais)
- Montagne x 27
- "Montagne" (Plaine) x 3

## Statistiques

## Valeurs de mana

- T : 166
- I : 98 (59 %)
- C : 68 (41 %)

### Dévotions

- R : 68 (100 %)

## Notes

### Todo

- tester / améliorer
