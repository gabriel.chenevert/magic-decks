# Cimetière vivant

## Commentaires

- Retiré car trop unidimensionnel (grosses créatures puis donner piétinement) et trop de cartes en commun avec Re-Animator
- Fusionné avec Sidisi qui permet de jouer aussi des grosses créatures avec l'accès au bleu (Lhurgoyf, Somnophage) mais plus de flexibilité (réanimation, protection) et orientation zombie

## Decklist


### Créatures (42)

- **Vieux brindodactyle**

- Araignée pêche-ciel

- Arbre-goule
- Augure de Llanowar
- Baloth piédetonnerre
- Bergère de hibours
- Briffaud d'âmes
- Brisecages de Kessig
- Carcasse fongiforme
- Chien de l'effroi
- Craintécharde
- Diplopode du moldgraf
- Dissident damné
- Druidesse du cercle de la terre
- Elfe de la charmille
- Elfes de Llanowar
- Émissaire viridian
- Essaim de moucharmes
- Gueulerasoir fouisseur
- Guidevoie satyre
- Guivre d'ossuaire
- Harponneur kraul
- Hurlenuit
- Jarad, seigneur liche des Golgari
- Jeune calotte de la mort // Énorme calotte de la mort
- La Force
- Le Vieux Rutstein
- Liche du Souterrègne
- Majordome mort-vivant
- Marionnette d'oronge brune
- Modeleur de monde
- Mycodraxe d'ossuaire
- Nécromasse grouillante
- Pillards golgari
- Porteur funéraire noueracine
- Prophétesse des crânes
- Ritualiste à fleurs de mort
- Rôdeur de boisblanc
- Seigneur de l'extinction
- Taxidermiste recluse
- Tisseuse de Nyx
- Vermine rongeuse


### Artefacts (6)

- Anneau solaire
- Autel de la démence
- Cachet d'ésotérisme
- Lame de l'aïeul
- Monument de Rhonas
- Talisman de résistance


### Enchantements (6)

- Altération de cimetière
- Infestation rampante
- Rancœur
- Scion d'Halaster
- Serres du bois sauvage
- Soulèvement de Garruk


### Rituels (6)

- En proie // Désespoir
- Noyade dans la fange
- Paillis
- Récolte de la meneuse de goule
- Recrutement effroyable
- Splendide restauration


### Éphémères (6)

- Horrible récupération
- Percée
- Puissance massive
- Remords accablants
- Ronger jusqu'à l'os
- Secrets du mausolée


### Terrains (34)

- Forêt x 12
- Marais x 10

- Argoth, sanctuaire de la nature
- Bosquet prospère
- Étendues sauvages en évolution
- Fondrière hantée
- Gorge des sylves
- Lande propère
- Margouillis du Marennois
- Svogthos, le Tombeau agité
- Temple de la maladie
- Tour de commandement
- Verger exotique
- Verger infâme


## Statistiques


### Valeurs de mana

- T : 195
- I : 116 (59,5 %)
- C : 79 (40,5 %)


## Dévotions

- G : 49 (62,0 %)
- B : 30 (38,0 %)


## Notes


### Todo

- Plus d'interaction ?

- Retirer un marais

- Plus de mana dorks ? Plus de terrains...

- Graveyard protection ?



### Add

- Moldervine reclamation
- Cachet de Golgari


### Want

- Angel of suffering
- Animate dead
- Back for seconds
- Blood for bones
- Bramble familiar
- Breach the multiverse
- Cauldron of eternity ?
- Crypt of agadeem
- Deadbridge chant
- Exhume
- Fiend artisan
- Golgari grave troll
- Greater good
- Grist
- Izoni
- Llanowar wastes
- Mortivore
- Necrotic ooze
- Nighthowler
- Noxious revival
- Nyléa, déesse de la chasse
- Perpetual timepiece
- Persist
- Primal rage
- Prodigious growth
- Ramunap excavator
- Reclamation sage
- Sakura tribe elder
- Song of Freyalise
- Songs of the damned
- Spider spawning
- Stinkweed imp
- Stitcher's supplier
- Tainted wood
- Tomb fortress
- Undergrowth stadium
- Varolz
- Vilespawn spider
- Woodland cemetary
- Woodland chasm
- Zopandrel
