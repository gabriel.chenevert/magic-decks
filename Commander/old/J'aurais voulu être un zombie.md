# J'aurais voulu être un zombie

- Démonté pour être fusionné avec Re-Animator, Dieu-Scarabée dans les 99

## Decklist


### Créatures (34)

- **Le Dieu-Scarabée**

- Âme d'Innistrad
- Archigoule de Thraben
- Boucher de Malakir
- Brigadier des damnés
- Chevalier putrefondrière // Perspicacité profane
- Chien de l'effroi
- Collectionneur d'organes
- Connaisseur en cadavres
- Création oubliée
- Démon scarifié de runes
- Esclavagiste de l'effroi
- Faucheur de l'abysse
- Fossoyeur abject
- Geralf, raccommodeur
- Geralf, raccommodeur visionnaire
- Gisa et Geralf
- Gisa, meneuse de goule
- Gorex, la carapace-tombe
- Goule du diregraf
- L'Émerveillement
- Lim-Dûl, le Nécromancien
- Ludevic, nécrogénie // Olag, orgueil de Ludevic
- Maître de la mort
- Majordome mort-vivant
- Marchand gris d'Asphodèle
- Médecin sinistre
- Momie miasmatique
- Nécromasse grouillante
- Opportuniste morbide
- Sangrophage
- Titan des tombes
- Tormod, le profanateur
- Zombie du cellier


### Rituels (12)

- À tout jamais
- Aperçu de l'inimaginable
- Apocalypse des zombies
- Brimades
- Chant du meneur de goule
- L'Âge de l’Éternité
- Obscur salut
- Ramper hors de la cave
- Recrutement effroyable
- Reviviscence
- Sélection nécromantique
- Soirée pour les goules


### Éphémères (4)

- Froideur de la tombe
- Hommage à Urborg
- Plaque d'armure en lazotèpe
- Remords accablants


### Enchantements (5)

- Nécrose goule
- Réserve du nécromancien
- Terrains d'entraînement
- Trahison de la tombe
- Tymaret appelle les morts


### Artefacts (8)

- Cachet d'ésotérisme
- Cachet de Dimir
- Cloche de meneur de goule
- Don du Dieu-Pharaon
- Grimoire des morts
- Portail vers l'au-delà
- Sablier perpétuel
- Talisman de dominance


### Planeswalker (1)

- Liliana, majesté de la mort


### Bataille (1)

- Invasion d'Amonkhet


### Terrains (35)

- Île x 8
- Marais x 19

- Aquifère contaminé
- Fondrière mortuaire
- Forteresse nécropole
- Margouillis du Marennois
- Marigot lugubre
- Ruisseau Ipnu
- Temple de la tromperie
- Tour de commandement


## Stats


### Coûts de mana

- T : 244
- I : 154 (63,1 %)
- C : 90 (36,9 %)


### Dévotions

- 71 B (78,9 %)
- 19 U (21,1 %)


## Notes


### Idées

- Dissident damné


### Try

- Scruter (surveiller)


### Recruitement de zombies

- Soul separator ?
- Vizier of Many Faces ??


### Objectifs

- Meuler
- Surveiller
- Zombie Friends
- Améliorer mana base
- Créatures pas fortes qui coûtent cher avec capacités / Petits zombies

### Wish

- Paradox haze (second upkeep)
- Kalitas, traître des Ghet
- Autel phyrexian
