# Rose noire

- Retiré à l'annonce de Fondations

## Decklist

### Créatures (28)

- **Vito, Épine de la Rose du crépuscule**

- Artiste de sang
- Ayara, première de Locthwain
- Blême des cryptes
- Charognard du diregraf
- Crisseur de la basilique
- Épicurien du sang
- Éthérien brûlant
- Filouteuse de taverne
- Goule piquée à l'argent
- Lampade de la veille des morts
- Marchand gris d'Asphodèle
- Plague Drone
- Présence effroyable
- Prétresse du fléau en maraude
- Sangramage inhumain
- Scorpion barbelé
- Scribe vampire
- Sengir, le Baron noir
- Servant de Tymaret
- Sheoldred, l'Apocalypse
- Suceur de sang rampant
- Surineur de Zulaport
- Tymaret, élu parmi les morts
- Vampire vindicative
- Vampirien
- Voltigeuse assoiffée de sang
- Vraan, Thane exécuteur

### Artefacts (7)

- Anneau solaire
- Bâton du magus de mort
- Cachet d'ésotérisme
- Corne de démon
- Masque de Griselbrand
- Monument de Bontu
- Reliquaire du fleuve Luxa

### Batailles (1)

- Invasion d'Ulgrotha // Grand-mère Ravi Sengir

### Enchantements (8)

- Bastion du souvenir
- Étreinte démoniaque
- Le massacre au croc de boucher
- Liaison vampirique
- Lien sanguin
- Sang délectable
- Siège du palais
- Vengeance des corbeaux

### Éphémères (14)

- Aide improbable
- Bénédiction de rage de bataille
- Droit à la gorge
- Emprise des ténèbres
- Exterminer
- Incursion dans la crypte
- Intervention d'Ashnod
- Messe noire
- Plongeon dans les ténèbres
- Précepteur vampirique
- Souiller
- Subir le passé
- Tu es déjà mort
- Vrilles de corruption

### Rituels (8)

- Contrainte
- Corruption
- Exsanguination
- Hommage de sang
- Infestation
- Sauvetage désespéré de Sam
- Signature de sang
- Vapeurs dévorantes

### Terrains (34)

- Marais x 31

- Coffres de la Coterie
- Nykthos, reliquaire de Nyx
- Urborg, tombe de Yaugzebul

### Jetons (1)

- Humain et soldat

## Statistiques

### Valeurs de mana

- T : 189
- I : 112 (59 %)
- C : 77 (41 %)

### Dévotions

- B : 77 (100 %)
