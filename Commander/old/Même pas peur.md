# Même pas peur

- Dans les limbes car joué juste une fois

## Stratégie

- Deck pauper commander
- Envoyer les zombies à l'abattoir avec l'Archigoule en jeu pour jouer d'autres zombies
- Réanimer les gros trucs quitte à en sacrifier des petits

## Decklist

### Créatures (38 dont 38 zombies)

- **Archigoule de Thraben**

- Anathème de Gavonie
- Armée sibsig
- Baudroie de Gurmag
- Béhémoth de Morkrut
- Boucher de Silumgar
- Bouffeur de charogne
- Carapace de nantuko
- Carapace-nécropole errante
- Champion des inféconds
- Chat noir
- Bête putréfiée vacillante
- Blême titubante 
- Crisseur charognard
- Exécuteur mort-vivant
- Charognard de Silumgar
- Corbeau de mauvais augure
- Faucheur d’étincelle
- Fossoyeur
- Gargantua de la marée putride
- Gobelin pourrissant
- Gobelin titubant
- Goule au rabais
- Horde du diregraf
- Goule bouchère
- Leveur de goule
- Momie miasmatique
- Officier pouacre
- Maraudeur sacpeau
- Marchand gris d'Asphodèle
- Médecin sinistre
- Momie en lambeaux
- Momie pourrissante
- Nécromasse grouillante
- Pue-sangs de Thraben
- Zombie boiteux
- Zombie de la Bourbière
- Zombie rongeur

### Rituels (10)

- Chant du meneur de goule
- Chuchotements nocturnes
- Échardes d'os
- Fragments d'os
- Ramper hors de la cave
- Nourrir l’essaim
- Reconstitution macabre
- Recrutement au cimetière
- Retour de l'effroi
- Retour titubant

### Éphémères (10)

- Barratement cadavérique
- Conviction corrompue
- Fatale glissade
- Homicide
- Offrir l'immortalité
- Renaissance abjecte
- Remords accablants
- Rites du village
- Souiller
- Terreur

### Artefacts (4)

- Diamant du charbon
- Babiole du voyageur
- Cloche de meneur de goule
- Sphère du commandant

### Enchantements (2)

- Courage fongique
- Expédition de l'Escalier aux âmes

### Terrains (36)

- Marais x 32

- Berceau des maudits
- Fondrière mortuaire
- Lande stérile
- Marécage de Bojuka

## Statistiques

### Valeurs de mana

- T : 170
- I : 105 (62 %)
- C : 65 (38 %)

### Dévotions

- B : 65 (100 %)

## Commentaires

### Todo

- Tester
- Conserver ?
- Plus de zombies, moins de sorts ?
