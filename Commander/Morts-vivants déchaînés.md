# Morts-vivants déchaînés

## Stratégie

- Sacrifier des zombies spour les faire revenir en décomposition
- Accumuler des jetons + réanimation massive

## Decklist

### Créatures (34) dont 34 zombies

- Adversaire souillé
- Archigoule de Thraben
- Augure mort-vivant
- Balafreur invincible
- Baron de la mort
- Blême titubante 
- Bontu l'Éternelle-déesse
- Bouffeur de charogne
- Capitaine du diregraf
- Cavalier sans tête
- Champion des trépassés
- Chef de guerre mort-vivant
- Colosse du diregraf
- Création oubliée
- Dralnu, seigneur liche
- Éructeur de peste
- Éternel seigneur céleste
- Faucheur de minuit
- Faucheur des cimetières
- Horde sans souffle
- Liche de Havengul
- Majordome mort-vivant
- Maraudeur maudit
- Mort vengeur
- Nécromasse grouillante
- Porte-étendard de Liliana
- Putréventre vorace
- Rampeur des tombes
- Seigneuresse des maudits
- Skaab au hachoir
- Tyran des tombes
- Vizir du Scorpion
- **Wilhelt, hacheur de pourriture**
- Zul Ashur, eigneuresse liche

### Planeswalkers (2)

- Liliana, épargnée par la mort
- Liliana, générale de la Horde de l'effroi

### Batailles (1)

- Invasion d'Innistrad // Déluge de morts

### Rituels (9)

- Apocalypse des zombies
- Armée des damnés
- Carnage impitoyable
- Conquête des chevaliers-liches
- Enterré vivant
- Maléfice nécrotique
- Ordre du patriarche
- Ultime vengeance
- Vider le laboratoire

### Éphémères (7)

- Déclencher la machine
- Levée de l'Effroyable armée de Marn
- Plaque d'armure en lazotèpe
- Remords accablants
- Réparation de cadavre
- Rites du village
- Sonder l'interdit

### Enchantements (2)

- Nécrodualité
- Orage sur le toit

### Artefacts (8)

- Anneau solaire
- Autel phyrexian
- Bûcher funéraire des héros
- Cachet d'ésotérisme
- Cachet de Dimir
- Cor du héraut
- Crypte surpeuplée
- Pincecrâne

### Terrains (37)

- Île x 6
- Marais x 20
- Catacombes de Sombreau
- Catacombes noyées
- Cour retirée
- Dépression engloutie
- Île souillée
- Marécage aux épaves
- Ossuaire submergé
- Rivière souterraine
- Terra nullius
- Tour de commandement
- Voie de l'Ascendance

### Jetons (6)

- Zombie / Zombie en décomposition x 6

## Stats

### Valeurs de mana

- T : 209
- I : 134 (64 %)
- C : 75 (36 %)

### Dévotions

- B : 61 (81 %) p = 3
- U : 14 (19 %) p = 2

### Taux de préco

- 51 %

### Dates

- Dernière partie : 2025-01- ?
- Dernière màj : 2025-02-22

## Notes

### Todo

- Plus de GY hate / boardwipe / counterspells
- Idées : commandants Dimir alternatifs comme Scarab God, Gisa & Geralf
- Feeling manque de zombies
- Arranger terrains

### Questions

- Tuteur enchantements ?
- Un peu moins de lords ?
- Champ des morts ? (créateur jetons)
- Takenuma, marais abandonné ? 12 €
- Un peu moins de bilands ?
- Évacuation des fosses ?

### Want

- Undead Warchief FR
- Zombie master
- Exécuteur réanimé (lord)
- Jeton réparation */* avec menace
- Living Death nice
- Rot Hulk
- Roaming Throne 25 €
- Poppet Factory
