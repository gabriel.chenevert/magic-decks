# Marqueurs congrégation

## Decklist

### Créatures (39)

- **Kyler, émissaire sigardien**

- Adeline, cathare resplendissante
- Ange de la gloire triomphante
- Avant-garde de Sigarda
- Avoquiste d’Orzhov
- Cathares au clairon
- Cavalier d'Odric
- Cavaliers de Gavonie
- Champion de la Grâce du héron
- Championne de Lambholt
- Chevalier de l’Orchidée blanche
- Éclaireuse vaillante
- Élite de la garde des dragons
- Élite Lame-héron
- Fauconnier abzan
- Garde d'écaille d’élite
- Gardebride perpétuelle
- Gardes de Cerf-orient
- Héraut de la guerre
- Leinore, souveraine de l’automne
- Lieurs d’âme des Custodi
- Maja, protectrice de Bretagard
- Mikaeus, le lunarque
- Odric, maître tacticien
- Pèlerin d’Avacyn
- Protectrice du bastion
- Ranger de l’Ordre du genévrier
- Représentante de la victoire
- Rick, Steadfast Leader
- Sage de Somberwald
- Saryth, croc de la vipère
- Sauvage maîtresse des bêtes
- Sauveteuse landéenne
- Sentinelle sigillaire
- Sigarda, fontaine de bénédictions
- Sigarda, grâce du héron
- Témoin éternel
- Vieux sage de la Yavimaya
- Zélateur sigardien

### Planeswalkers (1)

- Ajani au Grandcœur

### Rituels (9)

- Amélioration biosynthétique
- Célébrer les moissons
- Fureur d'Ent
- Jugement céleste
- Nova purificatrice
- Révélation shamanique
- Spasme de croissance
- Venez danser !
- Zénith de Vertsoleil

### Éphémères (8)

- Appel inspirateur
- Formation inébranlable
- Fuite de Loran
- Intrusion désastreuse
- Regain de la nature
- Rendre à la poussière
- Retour au pays
- Réveiller la bête

### Enchantements (2)

- Présence de la mort
- Siège de la citadelle

### Artefacts (6)

- Anneau solaire
- Bestiaire du façonneur de vie
- Bottes de piedagile
- Cachet d'ésotérisme
- Cor du Gondor
- Talisman d'unité

### Terrains (35)

- Forêt x 9
- Plaine x 12

- Comté de Gavonie
- Oran-Rief, le Vastebois
- Passage des malandrins
- Porte de la guilde de Selesnya
- Prairie de Solherbe
- Sanctuaire de Selesnya
- Sylves gangrenées
- Temple de la fausse divinité
- Temple de la profusion
- Tour de commandement
- Verger exotique
- Village fortifié
- Voie de l'Ascendance
- Vue de la canopée

### Jetons (21)

- Araignée x 3
- Bête x 2
- Centaure
- Chevalier
- Eldrazi et Engeance
- Élépphant
- Esprit x 3
- Humain
- Humain et Soldat
- Loup x 5
- Rinocéros
- Serpent

## Statistiques

### Coûts de mana

- T : 226
- I : 138 (61,1 %)
- C : 88 (38,9 %)

### Dévotions

- W : 51 (58,0 %)
- G : 37 (42,0 %)

### Préco

- 95 %
