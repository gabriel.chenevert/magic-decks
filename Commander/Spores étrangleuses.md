# Spores étrangleuses

## Decklist

### Créatures (36) dont 33+2 fongus

- **Thélon de Havrebois**

- Aruspice thallidé
- Chancreflore
- Changelin tissetoile
- Escargot fongicrâne
- Escogriffe de moisissure
- Feral Thallid
- Fongepied, le clandestin
- Jeune calotte de la mort // Énorme calotte de la mort
- Le Mycotyran
- Marcheroyaume
- Marionnette d'oronge brune
- Mycon d'utopie
- Myconide du Cellier
- Rampeur à spores
- Saproberger de la Yavimaya
- Spore Flower
- Spores d'autisme
- Sporoloss ancien
- Tertre aux spores
- Thallid Devourer
- Thallidé
- Thallidé à fleurs de mort
- Thallidé encoquillé
- Thallidé fritillaire
- Thallidé germinateur
- Thallidé mortespore
- Thallidé omnivore
- Thallidé psychotrope
- Thallidé sauvage
- Thallidé sèmespore
- Thallidé tukalanguier
- Thallidé vitaspore
- Thorn Thallid
- Veilleur de spores myconide
- Vrille du Mycotyran

### Éphémères (9)

- Accumulation
- Courage résonnant
- Feuillage immédiat
- Putréfier
- Renaissance fongoïde
- Rites du village
- Sinistre affliction
- Sonder l'interdit
- Spore Cloud

### Rituels (6)

- Croissance luxuriante
- Culture
- Extension de la sphère
- Moisissure rampante
- Mousse acide de la Mwônvouli
- Noyade dans la sanie

### Enchantements (7)

- Explosion de saprobiontes
- Fécondité
- Grappe de saprobiontes
- Poings de ferbois
- Reboisement de moiselierre
- Sporogenèse
- Survivre à la force des bras

### Artefacts (7)

- Anneau solaire
- Bûcher funéraire des héros
- Cachet d'ésotérisme
- Cachet de Golgari
- Icône de l'Ascendance
- Pierre de l’esprit
- Tambour de sautefeuille

### Terrains (35)

- Forêt x 21
- Marais x 6
- Dépression de jungle
- Étendues sauvages en évolution
- Fondrière hantée
- Marécage de Bojuka
- Porte de la guilde de Golgari
- Temple de la fausse divinité
- Tour de commandement
- Verger infâme

## Stats

### Coûts de mana

- T : 187
- I : 111 (59 %)
- C : 76 (41 %)

### Dévotions

- G : 61 (80 %) p = 3
- B : 15 (20 %) p = 1

## Notes

### Todo

- Focuser un peu plus (marqueurs spores)
- Plus de pioche / meulage / petits fongus
- Plus de terrains ?
- Que des forêts golgari
- Meuler / sacrifier pour fongus dans cimetière

### Questions

- À quel point on ignore les saprobiontes ?
- Feuillage immédiat
- Mycoloth ?
- Retirer fongus moyen pour remettre terrains
- Assez de moteurs à sacrifice ?

### À améliorer

- moins de sorts à 2
- plus d'effets tribaux
- tutorer combo Sporoloss / Sèmespore ?
- piocher / rampe / proliférer
- manque de rituels ? réduire créatures
- meuler des fongus ?
- protection pour Thélon

### Essayer

- Restauration surnaturelle ?
- Se battre bec et ongles ?
- Contrainte ?
- Tombeau luxuriant ?
- Regard de granit ?
- Parallel Lives ?
- Spread the Sickness ?
- Canopée carnivore ?
- Murmure de Mephidross
- Sentir la peur
- Contagion Clasp

## Want

- Winding Constrictor
- Pour les ancêtres ? 8 €
- Darksteel Plate
- Saison de dédoublement ?
- Cour retirée
- Trudge garden 
- Compost
- Cor du héraut
- Diabolique dessein
- Fungal Bloom
- Recrutement de congénères
- Traînailleur germéchine ?
- Trophée de l'assassin
- Underground Mortuary
