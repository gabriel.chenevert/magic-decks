# Elfes de la Noël

## Remarques

### Stratégie

- Poser des elfes à mana pour jouer vite Tyvar puis les faire grossir
- Attaquer facilement et essayer de leur donner piétinement

### Dates

- Dernières màj : 2025-01-14
- Dernière partie : 2025-01-16

## Decklist

### Créatures (32) dont 31 elfes

- Aberration elfe
- Abomination de Llanowar
- Admirateurs masqués
- Archer à l'arc épineux
- Archer aux pointes empoisonnées
- Archidruide elfe
- Druide du cercle des rêves
- Druidesse d'incubation
- Druidesse de clairpollen
- Druidesse de paradis
- Elfes cordelliens
- Elfes de Llanowar
- Franc-tireuse de Cielouvert
- Garde d'essence
- Guidevoie silhana
- Juge des façonneurs d'armures
- Marwyn, la nourricière
- Mystique elfe
- Négociant d'objets trouvés golgari
- Préservateur né des friches
- Prêtresse de Titania
- Rishkar, renégat de Peema
- Ritualiste à fleurs de mort
- Sage de l'évolution
- Sage de la spirale
- Sage du reboisement
- Seigneur de l'effroi elfe
- Storrev, liche des Devkarin
- **Tyvar le belliqueux**
- Vanneur impitoyable
- Yeva, héraut de la Nature

### Planeswalkers (1)

- Tyvar Kell


### Rituels (12)

- À toute vapeur
- Canopée carnivore
- Clairvoyance elfe
- Déferlement de Vastebois
- Don de Skemfar
- En proie // Désespoir
- Exsanguination
- Révélation shamanique
- Saison des récoltes
- Tourment de grêle de feu
- Vengeance du roi Harald
- Victimes de la guerre


### Éphémères (8)

- Appel inspirateur
- Carnage de la forêt
- Faim de l'oubli
- Murmure de Mephidross
- Position de Tyvar
- Puissance des masses
- Recrutement de congénères
- Scrotch !


### Enchantements (8)

- Chant de Freyalise
- Châtiment des ancêtres
- Croissance surnaturelle
- Écailles renforcées
- Harald unit les elfes
- Ligne ley d'abondance
- Prouesse des justes
- Soulèvement de Garruk


### Artefacts (4)

- Bannière du vainqueur
- Jarre d'âme du Grand serpent
- Monument de Rhonas
- Pierre d'horizon


### Terrains (35)

- Forêt x 22
- Marais x 11
- Temple de la maladie
- Voie de l'Ascendance

## Statistiques

### Valeurs de mana

- T : 196
- I : 104 (53 %)
- C : 92 (47 %)

### Dévotions

- G : 67 (73 %)
- B : 25 (27 %)


## Notes


### Todo

- manque rase-board
- un peu plus de marais ou taplands ?
- plus de terrains (recyclables)
- plus de support tribal
- plus de puits de mana ?
- un peu plus de créatures ?
- dégager + éphémères pour en profiter au tour des autres et bloquer


### Want

- Tyvar, Jubilant Brawler
- Elves of Deep Shadow
- Joraga Treespeaker
- Deathrite Shaman
- Shaman of the Pack
- Wirewood Lodges
