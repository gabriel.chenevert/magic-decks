# Rituels de sang

## Description

- Préco vampire intégrant quelques cartes de l'ancien Vito

## Decklist

### Créatures (36,5 dont 36 vampires)

- **Clavileño, premier parmi les bénis**
- Artiste de sang
- Artiste de sang (!)
- Bartolomé del Presidio
- Boucher de Malakir
- Carmen, cruelle marcheciel
- Champion du crépuscule
- Charognard faucon de nuit
- Chœur de la rédemption
- Conquérant charismatique
- Drana, libératrice de Malakir
- Elenda, la Rose du crépuscule
- Exécration de sang
- Fauteurs de troubles de la croisée
- Goule piquée à l'argent
- Hiérophante d'Elenda
- Lieutenante de la Légion
- Maîtresse des rites noirs
- Martyre du crépuscule
- Mavren Fein, Apôtre du crépuscule
- Nécromancien de la lignée
- Noble Falkenrath
- Officiante cruelle
- Porteur de la Loi impitoyable
- Quêteuse de sanctuaire
- Sengir, le Baron noir
- Sergent de la Légion du crépuscule
- Sorin de la maison Markov // Sorin, nouveau-né vorace
- Timothar, baron des chauves-souris
- Traqueur de sang
- Vampire assermenté
- Vito, Épine de la Rose du crépuscule
- Vito, fanatique d'Aclazotz
- Vona, bouchère de Magan
- Voyant de viscères
- Vraan, Thane exécuteur
- Yahenni, partisan immortel

### Planeswalkers (2,5)

- Sorin, seigneur d'Innistrad
- Sorin, seigneur de sang vengeur

### Rituels (5)

- Colère d'Olivia
- Commandement d'austérité
- Damner
- Pacte du Grand serpent
- Sang neuf

### Éphémères (2)

- Destruction totale
- Rites du village

### Artefacts (11)

- Anneau solaire
- Babiole du voyageur
- Bottes de piedagile
- Cachet d'ésotérisme
- Cachet d'Orzhov
- Lame d'héritage
- Lame du chef de sang
- Pierre de l'esprit
- Reliquaire de la Rose du crépuscule
- Sphère du commandant
- Talisman de hiérarchie

### Enchantements (6)

- Destinée radieuse
- Faveur de congénères
- Gravures des Élus
- Marche des canonisés
- Promesse d'Aclazotz // Renaissance infâme
- Sang délectable

### Terrains (37)

- Marais x 13
- Plaine x 8
- Basilique d'Orzhov
- Caveau de l'archange
- Champ souillé
- Chapelle isolée
- Château de Dracula (Domaine Voldaren)
- Cour retirée
- Hauts de Ventabrupt
- Lacis luminombre
- Marécage de Bojuka
- Passage des malandrins
- Paysage myriadaire
- Temple de la fausse divinité
- Temple du silence
- Terra nullius
- Tour de commandement
- Voie de l'Ascendance

## Statistiques

### Valeurs de mana

- T : 194
- I : 113 (58 %)
- C : 81 (42 %)

### Dévotions

- B : 54 (67 %) p = 2
- W : 27 (33 %) p = 2

### Taux de préco

- 90 %
