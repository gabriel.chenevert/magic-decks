// initial inspiration from https://www.reddit.com/r/magicTCG/comments/ff5ho9/using_scryfall_api_on_google_sheets/

function SCRYFALL_REQUEST(request) {

  var res;
  var base_url = "https://api.scryfall.com/";

  try {
    res = UrlFetchApp.fetch(base_url + request, {muteHttpExceptions: true});
  }

  // HTTP error

  catch(err) { return err; }

  const json = JSON.parse(res.getContentText());

  // Google error

  if (json.name == "Exception") { return "fetch error"; }

  // Scryfall error

  if (json.object == "error") { return json.code; }

  return json;

}

function GET_JSON(cardName) {

  return SCRYFALL_REQUEST("cards/named?fuzzy=" + encodeURI(cardName));

}

function MANA_VALUE(cardName) {

  return GET_JSON(cardName).cmc;

}

function EN_NAME(cardName) {

  return GET_JSON(cardName).name;

}

function EST_PRICE(cardName) {

  // add sort criterion?

  var json = GET_JSON(cardName);

  if (json.prices.eur) { return parseFloat(json.prices.eur); }

  if (json.prices.eur_foil) { return parseFloat(json.prices.eur_foil); }

  if (json.prices.usd) { return .91*parseFloat(json.prices.usd); }

  if (json.prices.usd_foil) { return .91*parseFloat(json.prices.usd_foil); }

  return "no_price";

}

function PAUPER_LEGAL(cardName) {

  var json = GET_JSON(cardName);

  return json.legalities.paupercommander;


}

function COLOR_ID(colors) {

  return colors[3];

}

PAUPER_LEGAL("Carrion Feeder");
