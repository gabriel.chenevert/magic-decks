# Géants

## Decklist

### Créatures (23)

- Aegar, la Flamme gelante
- Amplificatrice guitûke
- Analyste furtive
- Balmor, capitaine mage de bataille
- Chasseur bicéphale // Double rage
- Coureuse de vortex x 2
- Enjambeur poing-de-gel x 2
- Ermite maléfique // Geist bienveillant
- Escroc de givre x 3
- Expert de Lat-Nam
- Flagelleur mental
- Géant craque-os
- Géant fulminant
- Géant rixesang
- Graveuse d'axiome
- Invocateur de cyclone
- Porteur de calamité
- Pulvérisateur poing-de-fer x 2

### Éphémères (6)

- Folie furieuse x 2
- Incendiez la tour !
- Piège glaçant
- Rage monstrueuse
- Sous le pied

### Rituels (4)

- Blâme fulminant
- Piler
- Recherches sur le terrain x 2

### Terrains (27)

- Île x 11
- Montagne x 11
- Campus de Prismari
- Étendues sauvages en évolution x 2
- Falaises des eaux vives x 2

## Statistiques

### Valeurs de mana

- T : 81
- I : 47 (58 %)
- C : 34 (42 %)

### Dévotions

- R : 19 (56 %)
- U : 15 (44 %)
