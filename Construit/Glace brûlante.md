# Glace brûlante

## Résumé

- Deck de démarrage bien modifié

## Decklist

### Créatures (23)

- Aegar, la Flamme gelante
- Amplificatrice guitûke
- Archimage émérite
- Balmor, capitaine mage de bataille
- Cavalerie axgardienne x 2
- Chasseur bicéphale // Double rage
- Coureuse de vortex x 2
- Dragon rouge
- Embusqueur des Pics vacillants
- Ermite maléfique // Geist bienveillant
- Escroc de givre x 4
- Expert de Lat-Nam
- Fæ des souhaits // Exaucé
- Flagelleur mental
- Géant fulminant
- Invocateur de cyclone
- Porteur de calamité
- Serpent trou-de-ver

### Rituels (4)

- Blâme fulminant
- Piler
- Recherches sur le terrain x 2

### Éphémères (6)

- Folie furieuse x 3
- Incendiez la tour !
- Piège glaçant
- Rage monstrueuse

### Terrains (27)

- Île x 11
- Montagne x 11

- Étendues sauvages en évolution x 4
- Falaises des eaux vives

## Statistiques

### Valeurs de mana

- T : 78
- I : 45 (57,7 %)
- C : 33 (42,3 %)

### Dévotions

- R : 17 (51,5 %) p = 2
- U : 16 (48,5 %) p = 2
