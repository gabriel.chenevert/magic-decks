# Sorcière bien-aimée

## Stratégie

- Ne pas miser sur l'attaque mais plutôt la magie détournée

## Decklist


### Artefacts (8)

- Cachet de Golgari
- Chaudron bouillonnant x 3
- Chaudron d'envoûteuse x 2
- Four de l'envoûteuse
- Sacoche druidique


### Créatures (16)

- Acolyte de Karametra
- Dina, macéreuse d'âmes
- Disciple Croc-ancêtre
- Envoûteuse au cœur amer
- Envoûteuse cloaquebouillon x 3
- Envoûteuse de Dentcariée
- Garde d'essence
- Ingrédient réticent
- Ophiomancienne
- Sage du reboisement
- Triton à pustules x 3
- Vilaine des marécages


### Enchantements (3)

- Malédiction de l'intrus
- Malédiction de toiles collantes
- Tourment de scarabées


### Éphémères (5)

- Guérison de Pharika
- Infusion de vitalité x 2
- Nourrir le chaudron
- Toile de l'envoûteuse


### Rituel (4)

- Chair pour araignée
- Culture
- Décoction mortelle
- Vengeance de l'envoûteuse


### Terrains (22)

- Forêt
- Marais x 8

- Bois souillé x 2
- Dépression de jungle x 4
- Lacis nécrofleur
- Maison de l'envoûteuse x 4
- Temple de la maladie x 4

## Statistiques

### Coûts de mana

- T : 89
- I : 52 (58 %)
- C : 37 (42 %)

### Dévotions

- B : 26 (69 %)
- G : 11 (31 %)

## Notes


### Pas sûr

- Acolyte de Karametra

### Todo

- Faire plus de nourriture
- Retirer un chaudron ? Ajouter un triton ?
- Réanimer Triton ?


### Want

- Familier de chaudron
- Jeton serpent 1/1 noir
- Witch's Clinic
- Witch of the Moors
