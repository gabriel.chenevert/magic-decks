# Bébé gobelin

## Decklist

### Créatures (24)

- Banneret gobelin
- Brandon fanatique
- Chef de guerre gobelin x 2
- Chipeur de trésor
- Commandant des assiégeants
- Coureur de pièges gobelin
- Finaude de la rue d'étain
- Gobelin hargneux
- Incinérateur gemmepaume
- Instigateur gobelin
- Krenko le caïd x 2
- Krenko, baron de la rue d’étain
- Larbin gobelin
- Matrone gobeline
- Meneur gobelin
- Patron de guerre de la Légion
- Skwi, l'immortel
- Taquineur effronté
- Tournoyeur de chaînes gobelin
- Traceur gobelin
- Vétéran de la volée x 2


### Artefacts (4)

- Bannière héraldique
- Cor du héraut
- Icône de l'Ascendance
- Rotefeu gobelin


### Enchantements (1)

- Assaut gobelin


### Éphémères (4)

- Bûcher funéraire cathartique
- Foudre
- Rituel de Clairepierre
- S'amuser avec le feu


### Rituels (1)

- Étreinte de lave


### Terrains (26)

- Montagne x 25
- Nykthos, reliquaire de Nyx

## Statistiques

### Valeurs de mana

- T : 85
- I : 52 (61 %)
- C : 33 (39 %)

### Dévotions

- R : 33 (100 %)

## Notes

### Want

- Chandra, pyromancienne novice
- Fureur des descendants ?
- Montagne M21 312 trop belle
- Montagne UND 94s
