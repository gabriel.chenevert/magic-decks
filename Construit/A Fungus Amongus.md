Deck
3 Fongepied, le clandestin (DAR) 205
7 forêt (ONE) 276
8 marais (MID) 382
4 Thallidé fritillaire (DAR) 181
3 Migration de saprobiontes (DAR) 178
2 Thallidé omnivore (DAR) 106
3 Infection fongoïde (DAR) 94
3 Thallidé à fleurs de mort (DAR) 84
2 Saproberger de la Yavimaya (DAR) 189
2 Essaim de spores (DAR) 180
4 Bastion du souvenir (IKO) 73
4 Dina, macéreuse d'âmes (STX) 178
3 Scorpion barbelé (IKO) 99
4 Dépression de jungle (M21) 247
3 Échardes d'os (MH2) 76
2 Chasse aux spécimens (STX) 73
3 Clairière des oronges brunes (VOW) 261
1 Don de l'alchimiste (M21) 87
1 Bénédiction de Belzenlok (DAR) 77
1 Infusion d'essence (STX) 69
1 Instant de défiance (BRO) 108

Réserve
3 Brèche du confinement (STX) 125
3 Invocation de nuisibles (STX) 211
