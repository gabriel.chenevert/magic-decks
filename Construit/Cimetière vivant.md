## Todo

### Remettre

Jardinière de fleurs de morts
2 Opportuniste morbide
Cour retirée

### Want

Titania, Voix de Gaia
Boseiju, celui qui résiste
Clairière des oronges brunes (9 voire 17 €)

Liche du souterrègne

Bête palustre florissante ?

## En attente

### Now

70 cartes dont 18 proxies (74,3 % vraies)

### Todo

Vrais jetons : ours, insectes

### À tester

Récupérer

Desceller la nécropole

Sheoldred MOM

-> Consécrateur gargouillant

Sigille de Myrkul (Alchimie)

### Post-rotation

Autel de la démence

Carcasse fongiforme

Cultivateur d'oronges brunes

Combat mortel

Élite de Liliana

Corbeau de mauvais augure

Approvisionneuse du raccomodeur

Bortuk Oscrécelle

Lord of Extinction

Mort vivante

Boneyard Mycodrax

Guivre viguespore

Svogthos, le Tombeau agité

Jarad, seigneur liche des Golgari (commandant !)

Chant de mortepont

Blessure nécrotique

Vigor Mortis

Trouvaille

Horrible récupération

Élite de Liliana


### MTGA

5 forêt (MID) 384
2 Jeune calotte de la mort (MID) 181
2 Vieux brindodactyle (MID) 234
2 Dissident damné (VOW) 106
1 Majordome mort-vivant (VOW) 133
3 Chien de l'effroi (MID) 97
4 Clairière des oronges brunes (VOW) 261
1 Essaim de moucharmes (DMU) 81
2 Opportuniste morbide (MID) 113
1 Remords accablants (BRO) 110
2 Rôdeur de boisblanc (BRO) 172
4 Taxidermiste recluse (VOW) 214
1 Infestation rampante (VOW) 193
1 Uurg, rejeton de Turg (DMU) 225
1 Nécromasse grouillante (DMU) 115
1 Altération de cimetière (SNC) 69
1 Gueulerasoir fouisseur (BRO) 173
1 Récolte de la meneuse de goule (MID) 225
3 Lhurgoyf d'Urborg (DMU) 186
2 Argoth, sanctuaire de la nature (BRO) 256
2 Titania, Voix de Gaia (BRO) 193
1 Ritualiste à fleurs de mort (BRO) 208
2 Araignée pêche-ciel (BRO) 221
1 Boseiju, celui qui résiste (NEO) 266
8 marais (MID) 382
1 Tyvar, bagarreur allègre (ONE) 218
1 Cour retirée (NEO) 275
1 Orateur du terreau de Llanowar (DMU) 170
1 Jardinière de fleurs de mort (DMU) 159
1 Vermine rongeuse (BRO) 101
1 Graine d'espoir (MOM) 204
1 Déchirer le Multivers (MOM) 94
1 Guérillera volante (MOM) 105
1 Ombre de sanie (MOM) 112
1 Desceller la nécropole (MOM) 128
1 Vorinclex (MOM) 213
1 Wrenn et le Briseroyaume (MOM) 217
