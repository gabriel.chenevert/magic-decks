# Rats

## Decklist

### Créatures (24 dont 14 rats)

- Aigrefin chargeur
- Avare aux moucherons
- Boucher de Seigneur Grouilleur
- Chasseur de rats
- Chat de gouttière d'entrepôt
- Colonie emmêlée
- Égoutier bonnet-rouge
- Envoûteuse des égouts corrompue
- Garna, poing-de-sang de Keld
- Karumonix, le roi des rats
- Rat au ventre de peste
- Ratier en lambeaux
- Rats de Rajh
- Rats du Trou d'Enfer
- Rats typhoïdiens
- Rats voraces
- Seigneur des bruyants ogre
- Seigneur Grouilleur, roi des égouts
- Seigneur ogre de quartier insalubre
- Stagiaire chasseuse de rats // Problème de parasites
- Totentanz, joueur de flûte
- Vague de rats
- Vermine rongeuse
- Vermine vorace

### Artefacts (2)

- Bannière héraldique
- Cachet de Rakdos

### Enchantements (2)

- Bénédiction de seigneur Grouilleur
- Transactions nocturnes

### Éphémères (6)

- Charme de Rakdos
- Fatale glissade
- Foudre
- Incendiez la tour !
- Lancer de rat
- Sang calcinant

### Rituels (5)

- Chanson de Totentanz
- Horrible destin
- Propagation de vermine
- Rats de laboratoire
- Réaction en chaîne

### Terrains (25)

- Marais x 12
- Montagne x 11
- Cairn de crânes de l'Immersturm
- Temple de la malice

## Statistiques

### Valeurs de mana

- T : 66
- I : 34 (52 %)
- C : 32 (48 %)

### Dévotions

- B : 23 (72 %)
- R : 9 (28 %)

## Notes

### Todo

- Rééquilibrer terrains (moins montagnes / plus marais)