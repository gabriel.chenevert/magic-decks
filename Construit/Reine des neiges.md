# Reine des neiges


## Résumé

- Geler les créatures de l'adversaire
- Le tuer


## Decklist


### Créatures (17)

- Clique des fileuses // Déchirer les coutures
- Farfadette maîtresse entraveuse x 2
- Farfadette moqueuse x 2
- Garde du gèlepont
- Gardeclé d'Argenfin
- Reine des glaces // Fureur de l'hiver
- Renne à toison de gel x 3
- Sentinelle forgée de glace x 2
- Sharae des profondeurs paralysantes x 2
- Strix busard
- Transmutateur de Vantress // Malédiction croassante


### Rituels (3)

- Berceuse d'Eriette x 2
- Congelé sur place


### Éphémères (7)

- Botte dédaigneuse
- Briser le sort
- Crachotage de sort
- Dégel
- Fripouilles frappées
- Plonger dans l'hiver x 2
- Succomber au froid


### Enchantements (6)

- Arrêt à froid
- Froid cinglant
- Sanctuaire solitaire x 4


### Artefacts (2)

- Couronne d'hiver de Hylda
- Lasso-tonnerre


### Terrains (24)

- Île x 14
- Plaine x 6

- Étendues sauvages en évolution x 2
- Porte de la guilde d'Azorius x 2


## Statistiques

### Valeurs de mana

- T : 67
- I : 41 (61 %)
- C : 26 (39 %)

### Dévotions

- U : 17 (65 %)
- W : 9 (35 %)


## Notes


### Différence MTGA

- Froid cinglant <-> Arrêt cinglant
- Sentinelle forgée de glace <-> Reine à toison de gel

- Plonger dans l'hiver +1
- Succomber au froid +1
- Hylda +2
- Plage désertée +4
- Étendues sauvages en évolution +2

- Plaine -1
- Île -4
- Fripouilles frappées -1
- Porte de la guilde d'Azorius -2
- Reine des glaces -1

### Todo

- plus de reine des neiges, moins de fées
- Jouer avec pour améliorer l'équilibre
- Ajouter du dégagement pour pouvoir engager plus
- Lasso est f'n bon / couronne aussi


### Want

- Hyslda
